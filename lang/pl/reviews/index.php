<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['externalexaminerarea'] = 'domena egzaminatora zewnętrznego';//area??
$string['expired'] = '&lt;wygasły&gt; - nadal możesz przeglądać arkusze i zapoznać się z %s działaniami/odpowiedziami na Twoje komentarze.';
$string['externalexamineraccess'] = 'Dostęp dla egzaminatora zewnętrznego';
$string['notreviewed'] = 'Nieprzejrzane';
$string['reviewed'] = 'Przejrzany: %s';
$string['msg1'] = 'Poniższa lista przedstawia arkusze wymagające recenzji. Po wybraniu tytułu arkusza, w nowym oknie przedstawiona będzie ocena. Pod każdym pytaniem znajdują się trzy przyciski pozwalające wartościować pytania: 1) <span style="background-color:#C5E0B3; color:#375623">&nbsp;Pytanie&nbsp;poprawne&nbsp;</span> (Domyślne), 2) <span style="background-color:#FFE599; color:#7F6000">&nbsp;Małe/nieliczne&nbsp;problemy&nbsp;</span> lub 3) <span style="background-color:#FF9090; color:#800000">&nbsp;Duże/liczne&nbsp;problemy&nbsp;</span>. Dostępne jest także pole tekstowe przeznaczone do odnotowania konkretnych uwag co do poprawy pytania.';
$string['msg2'] = 'Przyciski nawigacyjne przemieszczające pomiędzy ekranami można znaleźć na dole ekranu. W niniejszym trybie recenzji dla wszystkich pytań wyświetlane są poprawne odpowiedzi (studenci otrzymają arkusze bez odpowiedzi). Arkusz można aktualizować wielokrotnie. Komentarze będą automatycznie zapisane podczas przechodzenia pomiędzy ekranami i po wybraniu \'Zakończ\'.';
$string['msg3'] = 'Poniżej znajduje się lista egzaminów dostępnych do recenzowania wyników grupy studentów. Pierwszy ekran przedstawia listę ocen z arkusza. Po kliknięciu na nazwisko studenta przedstawione będzie okno ze skryptem studenta pokazujacym szczegółowe odpowiedzi i oceny cząstkowe.';
$string['nopapersfound'] = 'Nie znaleziono żadnego arkusza!';
$string['copyrightmsg'] = 'Pytania zawarte w Rogō są chronione prawem autorskim i należą do %s.';
$string['helpandsupport'] = 'Pomoc i wsparcie';
$string['helpandsupportext'] = 'Pomoc i wsparcie dla zewnętrznych egzaminatorów recenzujących arkusze w Rogō.';
$string['rogodetails'] = 'Rogō %s jest elektronicznym systemem egzaminacyjnym open source przygotowanym przez Information Services Uniwersytetu Nottingham.<br />Więcej szczegółów poznasz na stronie projektu:';
$string['onlinesupportsystem'] = 'System pomocy online dla studentów';
$string['email'] = 'Email'; //cognate
$string['deadline'] = 'Termin finalizacji:';
$string['notset'] = '&lt;brak&gt;';
$string['preexamreviewpapers'] = 'Arkusze przeglądu przedegzaminacyjnego';
$string['postexamreviews'] = 'Przeglądy poegzaminacyjne';$string['papersforreview'] = 'Papers for Review';
$string['msg4'] = 'Below is a list of exam papers requiring review.  In the paper, beneath each question are four buttons for you to rate the question: 1) <span style="background-color:#C5E0B3">&nbsp;Question&nbsp;OK&nbsp;</span>, 2) <span style="background-color:#FFE599">&nbsp;Minor/some&nbsp;problems&nbsp;</span>, 3) <span style="background-color:#FF9090">&nbsp;Major/several&nbsp;problems&nbsp;</span> or 4) <span style="background-color:#C0C0C0">&nbsp;Cannot Comment&nbsp;</span> for any questions outside of your field of expertise. There is also a textbox to directly record your feedback to us where you feel there are points to raise.';

?>