<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/paper/details.php';

$string['start'] = 'Start';
$string['owner'] = 'Vlastník';
$string['question'] = 'Úloha';
$string['type'] = 'Typ';
$string['marks'] = 'Známky';
$string['modified'] = 'Upraveno';
$string['passmark'] = 'Potřebná známka';
$string['mappingbyyear'] = 'Mapování dle roku';
$string['bysession'] = 'dle relace';
$string['byquestion'] = 'dle úlohy';
$string['longitudinal'] = 'Dlouhodobě';
$string['screen'] = 'Obrazovka';
$string['warning'] = 'Varování';
$string['nomatchsession'] = 'Datum v Názvu Testu (%s) neodpovídá sezení testu (%s).';
?>