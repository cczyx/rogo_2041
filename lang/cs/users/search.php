<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';
require '../lang/' . $language . '/include/titles.inc';

$string['usermanagement'] = 'Nastavení uživatelů';
$string['usersearch'] = 'Hledání uživatelů';
$string['msg1'] = "<strong>Pozor</strong>&nbsp;&nbsp;&nbsp; nemáte žádnou specifikaci kategorie hledání v 'Rozšířených' parametrech.";
$string['msg2'] = 'V rámci zadaných kritérií nebyl nalezen žádný odpovídající uživatel';
$string['performsummary'] = 'Přehled výsledků';
$string['largeresult'] = 'Byl nalezen velký počet odpovídajících položek, zobrazeno je prvních 10,000.'
  . 'Specifikujte, prosím, podrobněji.';
?>