<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['statistics'] = 'Statistika';
$string['questionsbyschool'] = 'Úlohy dle Školy';
$string['school'] = 'Škola';
$string['info'] = 'Info blok';
$string['keyword_based'] = 's Klíčovým slovem';
$string['random'] = 'Náhodný blok';
$string['dichotomous'] = 'Dichotomie';
$string['extmatch'] = 'EMQ';
$string['blank'] = 'Doplňovací';
$string['hotspot'] = 'Image Hotspot';
$string['labelling'] = 'Štítkování';
$string['likert'] = 'Likert';
$string['matrix'] = 'Matrix';
$string['mcq'] = 'MCQ';
$string['mrq'] = 'MRQ';
$string['rank'] = 'Přiřazování';
$string['sct'] = 'SCT';
$string['textbox'] = 'Textbox';
$string['true_false'] = 'Ano/Ne';
$string['area'] = 'Plocha';
$string['enhancedcalc'] = 'Výpočet';
$string['flash'] = 'Úloha využívající Flash';
?>