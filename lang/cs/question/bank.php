<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/question_types.inc';
require_once '../lang/' . $language . '/include/paper_types.inc';
require_once '../lang/' . $language . '/include/blooms.inc';

$string['questionbank'] = 'Banka úloh';
$string['bytype'] = 'dle Typu';
$string['byblooms'] = 'dle Bloomovy taxonomie';
$string['bykeyword'] = 'dle Klíčového slova';
$string['bystatus'] = 'dle Statusu';
$string['bydifficulty'] = 'dle Obtížnosti';
$string['bydiscrimination'] = 'dle Diskriminace';
$string['byperformance'] = 'dle Výkonu';
$string['byobjective'] = 'dle Výukových cílů';
$string['manageobjectives'] = 'Správa Cílů';
$string['managekeywords'] = 'Správa Klíčových slov';
$string['referencematerial'] = 'Referenční materiály';
$string['nokeywords'] = 'Do tohoto Modulu nebyla přidána žádná Klíčová slova..';
$string['question'] = 'Úloha';
$string['questions'] = 'Úloh';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Lidé';
$string['noquestions'] = 'V Bance nebyly nalezeny žádné úlohy.';
$string['noquestionsbloom']  = 'Nebyla nalezena žádná úloha na základě vyhledávání "dle Bloomovy taxonomie".';
$string['noquestionsstatus'] = 'Nebyla nalezena žádná úloha na základě vyhledávání "dle Statusu".';
$string['noquestionskeyword'] = 'No questions found in bank by keyword.';
$string['noquestionsperformance'] = 'Nebyla nalezena žádná úloha na základě vyhledávání "dle Výkonu".';
$string['noquestionsobjective'] = 'Nebyla nalezena žádná úloha na základě vyhledávání "dle Cílů".';
$string['search'] = 'Hledat';
?>