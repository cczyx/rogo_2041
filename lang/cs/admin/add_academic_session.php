<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.


$string['addacademicsession'] = 'Nová Akademická relace';
$string['calendaryear'] = 'Kalendářní rok';
$string['academicyear'] = 'Akademický rok';
$string['calstatus'] = 'Povolit v Kalendáři';
$string['statstatus'] = 'Povolit ve Statistice';
$string['save'] = 'Uložit';
$string['cancel'] = 'Zrušit';
$string['academicsessions'] = 'Akademická Relace';
$string['duplicateerror'] = 'Kalendářní rok již existuje.';
$string['createsession'] = 'Nová Akademická Relace';
$string['editsession'] = 'Upravit Akademickou Relaci';
$string['deletesession'] = 'Odstranit Akademickou Relaci';
$string['calendaryear_tt'] = 'např. 2015, 2016';
$string['academicyear_tt'] = 'např. 2015/16, 2015/2016, 15/16, 15-16';
$string['calendarenabled_tt'] = 'Zobrazí se Akademická relace v Kalendáři?';
$string['statenabled_tt'] = 'Zobrazí se Akademická relace ve Statistikách?';
$string['invalidcalendaryear'] = 'Invalid calendar year';