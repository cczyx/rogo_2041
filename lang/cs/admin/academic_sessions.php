<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['academicsessions'] = 'Akademická Relace';
$string['calendaryear'] = 'Kalendářní rok';
$string['academicyear'] = 'Akademický rok';
$string['calendarenabled'] = 'Povolit v Kalendáři';
$string['statisticsenabled'] = 'Povolit ve Statistice';
$string['musthavesession'] = 'Nastavte, prosím, Relaci (nastavení Relace je důležité pro správný chod funkce)';
$string['createsession'] = 'Nová Akademická Relace';
$string['editsession'] = 'Upravit Akademickou Relaci';
$string['deletesession'] = 'Odstranit Akademickou Relaci';