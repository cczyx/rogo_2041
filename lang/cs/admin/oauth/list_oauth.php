<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['oauthkeys'] = 'Autorizační klíče';
$string['expires'] = 'Platný do';
$string['username'] = 'Uživatelské jméno';
$string['deleteoauthkeys'] = 'Odebrat autorizační klíč';
$string['listoauthclient'] = 'Seznam klientů';
$string['client'] = 'Klient';
$string['oauthclients'] = 'Klienti';
$string['type'] = 'Typ';
$string['token'] = 'Token';