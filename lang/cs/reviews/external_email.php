<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['emailtemplate'] = 'Emailová šablona';
$string['to'] = 'Komu';
$string['cc'] = 'Kopie';
$string['bcc'] = 'Skrytá kopie';
$string['subject'] = 'Předmět';
$string['subject_msg0'] = 'Komentář k e-testu %s ';
$string['subject_msg1'] = 'PŘIPOMÍNKA: recenze dokumentu %s';
$string['subject_msg2'] = '%s prohlédnout komentáře';
$string['emailsent'] = 'Email byl odeslán.';
$string['back'] = '&lt zpět';
$string['email'] = 'E-mail';
$string['noexaminers'] = 'Pro externí examinátory nebyl nastaven žádný termín uzávěrky.';
?>