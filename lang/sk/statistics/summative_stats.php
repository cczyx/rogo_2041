<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/months.inc';

$string['summativeexamstats'] = 'Sumatívne skúšky';
$string['statistics'] = 'Štatistika';
$string['month'] = 'mesiac';
$string['papers'] = 'Dokumenty';
$string['owner'] = 'Vlastník';
$string['mean'] = 'Priemer';
$string['min'] = 'Min';
$string['max'] = 'Max';
$string['studentpapers'] = 'Študentov/Dokumentov';
$string['cohortsizes'] = 'Veľkosť skupiny';
$string['uniquestudents'] = 'Unikátných študentov = %s';
$string['totals'] = 'Súhrny';
$string['students'] = 'Študentov';
$string['taken'] = 'Zadaných';
$string['unused'] = 'Nepoužitých';
$string['computerlab'] = 'Počítačová učebňa';
$string['examno'] = 'Číslo skúšky';
?>