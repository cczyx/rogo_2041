<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['statistics'] = 'Štatistika';
$string['questionsbyschool'] = 'Úlohy podľa školy';
$string['school'] = 'Škola';
$string['info'] = 'Informačný blok';
$string['keyword_based'] = 'Úloha s kľúčovým slovom';
$string['random'] = 'Náhodný blok';
$string['dichotomous'] = 'Dichotómia';
$string['extmatch'] = 'EMQ';
$string['blank'] = 'Doplň prázdne';
$string['hotspot'] = 'Image Hotspot';
$string['labelling'] = 'Štítkovanie';
$string['likert'] = 'Likertova škála';
$string['matrix'] = 'Matica';
$string['mcq'] = 'MCQ';
$string['mrq'] = 'MRQ';
$string['rank'] = 'Priradzovanie';
$string['sct'] = 'SCT';
$string['textbox'] = 'Textové pole';
$string['true_false'] = 'Pravda/Nepravda';
$string['area'] = 'Plocha';
$string['enhancedcalc'] = 'Výpočtová úloha';
$string['flash'] = 'Flash Interface';
?>