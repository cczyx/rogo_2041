<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['gender'] = 'Pohlavie';
$string['title'] = 'Titul';
$string['surname'] = 'Priezvisko';
$string['firstnames'] = 'Meno';
$string['studentid'] = 'ID študenta';
$string['course'] = 'Kurz';
$string['year'] = 'Rok';
$string['submitted'] = 'Podané';
$string['started'] = 'Začiatok';
$string['correctanswers'] = 'Správne odpovede ->';
$string['nodata'] = 'V danom období skúšku neurobil žiadny študent';
$string['bonus'] = 'Bonus';
?>