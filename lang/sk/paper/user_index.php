<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_security.inc';

$string['startscreen'] = 'Úvodná obrazovka';
$string['availability'] = 'Dostupnosť:';
$string['to'] = 'až';
$string['candidates'] = 'Kandidáti:';
$string['screens'] = 'Obrazovky:';
$string['marks'] = 'Hodnotenia:';
$string['adjusted'] = 'Nastavené z náhodných hodnôt';
$string['currentuser'] = 'Aktuálny používateľ:';
$string['timeremaining'] = 'Zostávajúci čas:';
$string['navigation'] = 'Navigácia:';
$string['bidirectional'] = 'Obojsmerná';
$string['unidirectional'] = 'Jednosmerná';
$string['start'] = 'Štart';
$string['restart'] = 'Reštart';
$string['previouscompletions'] = 'Predchádzajúce ukončenie:';
$string['rubric'] = 'Rubrika:';
$string['duration'] = 'Trvanie:';
$string['testclip'] = 'Ukážka';
$string['switchpapers'] = 'Prepnúť dokumenty';
$string['papernotavailable'] = 'Dokument nie je momentálne dostupný.';
$string['papernotavailablestudents'] = 'Dokument nie je momentálne študentom dostupný.';
$string['nottakenpaper'] = '(Doteraz ste si tento dokument nevybral.)';
$string['donotstart'] = "<strong>Neklikajte</strong> na 'Štart' pokým nedostanete pokyn od vyučujúceho.";
$string['hour'] = 'hodina';
$string['hours'] = 'hodiny';
$string['minutes'] = 'minúty ';
$string['mins'] = 'minút';
$string['notset'] = 'nenastavené';
$string['secs'] = 'sekúnd';
$string['papernotfound'] = 'Dokument nebol nájdený.';
$string['requestedpaper'] = 'Požadovaný dokument nebol nájdený.';
$string['guestaccount'] = 'Dočasný účet';
$string['metadata_msg'] = "Vaše '%s' nesúhlasí s '%s'.";
$string['timeexpired'] = 'Zostávajúci čas vypršal.';
$string['tooltip_bidirectional'] = 'Máte povolené sa medzi obrazovkami pohybovať dopredu aj dozadu.';
$string['tooltip_unidirectional'] = 'Akonáhle potvrdíte svoje odpovede odoslaním formulára, nebudete sa môcť vrátiť späť.';
$string['tooltip_adjustmark'] = 'Vaše percentá budú po skúške automaticky upravené podľa náhodnej známky, ktorú by získal študent, ak by odpovedal celkom náhodne. Z tohto dôvodu Vám teda doporučujeme zodpovedať všetky úlohy.';
$string['tooltip_testclip'] = 'Toto je testovací zvukový klip, slúžiaci pre správne nastavenie slúchadiel a hlasitosti.';
$string['photoid'] = 'ID fotografie';
?>