<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_types.inc';

$string['createnewpaper'] = 'Nový dokument';
$string['papertype'] = 'Typ dokumentu';
$string['name'] = 'Názov';
$string['next'] = 'Ďalší >';
$string['msg1'] = 'Vyberte, prosím, typ dokumentu, ktorý si prajete vytvoriť.';
$string['msg2'] = 'Zadajte, prosím, zmysluplný názov tohto dokumentu.';
$string['msg3'] = 'Summative exams are scheduled by an external system are you sure you want to continue?';
?>