<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['noexamsfound'] = 'Neboli nájdené žiadne skúšky';
$string['exams'] = 'Skúška';
$string['staffmangscreens'] = 'Manažér';
$string['note1'] = '<strong>Poznámka:</strong> Toto je stránka slúžiaca na sumatívne testovanie študentov, chceli ste vstúpiť na stránku';
$string['unknownip'] = '(neznáma adresa)';
$string['mostLikely'] = 'Najpravdepodobnejšou príčinou je jeden alebo viacero bezpečnostných konfliktov s:';
$string['IPaddress'] = 'IP adresa';
$string['Time/Date'] = 'Čas/Dátum';
$string['yearofstudy'] = 'Rok štúdia';
$string['noyear'] = 'používateľ nemá nastavený rok!';
$string['Modules'] = 'Moduly';
$string['nomodules'] = 'Varovanie: nie je registrovaný žiadny modul!';
$string['UserRoles'] = 'Používateľské role';
$string['try'] = 'Čo si môžete vyskúšať';
$string['f5'] = 'Ak je viac ako 15 minút pred začiatkom skúšky, prosím, vyčkajte až bude do začiatku ostávať 15 minút a stlačte kláves F5.';
$string['RaiseYourHand '] = 'Zdvihnite ruku a prihláste sa dozorujúcim osobám.';
$string['staff'] = 'Zamestnanec';
$string['student'] = 'Študent'; 
$string['sysadmin'] = 'Systémový správca';
$string['admin'] = 'Správca';
$string['graduate'] = 'Absolvent';
$string['inactive'] = 'Neaktívny';
$string['multipleExams'] = 'Nájdených viacero skúšok';
$string['selectOne'] = 'Vyberte, prosím, tú, ktorú si prajete zložiť:';
$string['passwordRequired'] = 'požadované heslo';
$string['screen'] = 'Obrazovka';
$string['screens'] = 'Obrazovky';
$string['Bidirectional'] = 'Obojsmerná navigácia';
$string['Unidirectional'] = 'Jednosmerná navigácia';
$string['requirespassword'] = 'Tento dokument vyžaduje heslo.';
$string['mins'] = 'minút';
$string['summativetesting'] = 'Sumatívne testovanie';
$string['summativetestmsg'] = '<strong>Len pre zamestnancov &ndash;táto časť nie je študentom viditeľná.</strong> Jedná sa o sumatívne skúšky, naplánované pre Vaše tímy a prebiehajúce v nasledujúcich šiestich týždňoch.';
$string['nodurationwarning'] = 'Varovanie: nie je nastavená dĺžka trvania skúšky';
$string['startwarning'] = 'Varovanie: dokument začína pred %d:00';
$string['nolabswarning'] = 'Varovanie: nie je nastavená žiadna učebňa';
$string['finished'] = 'Dokončené';
?>