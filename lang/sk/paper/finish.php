<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_security.inc';
require_once '../lang/' . $language . '/question/sct_shared.php';

$string['norights'] = 'Na zobrazenie tohto dokumentu nemáte oprávnenie.';
$string['examscript'] = 'Skript skúšky';
$string['error_paper'] = 'Nie je možné nájsť požadovaný dokument.';
$string['thankyoumsg'] = 'Ďakujeme Vám za vyplnenie <strong>%s</strong>. Vaše odpovede boli zaznamenané.';
$string['studentviewend'] = 'Týmto textom končí zobrazenie viditeľné pre študenta';
$string['staffviewbelow'] = '<strong>Toto je zobrazenie pre zamestnanca </strong>(študenti ho nevidia)';
$string['answersscreen'] = 'Obrazovka odpovedí';
$string['key'] = 'Kľúč:';
$string['correctanswer'] = 'Správna odpoveď';
$string['incorrectanswer'] = 'Chybná odpoveď';
$string['boldwords'] = "<strong>Tučné</strong> slová reprezentujú správne odpovede ku každej z úloh (nie odpovede študentov).";
$string['feedbackinred'] = 'Komentár je zobrazený tmavo červenou kurzívou';
$string['note'] = 'Poznámka';
$string['mousereveal'] = 'Pre zobrazenie správnej odpovede prejdite myšou nad značku chyby';
$string['screen'] = 'Obrazovka %s z %s';
$string['unanswered'] = 'Nezodpovedané';
$string['summaryofmarks'] = 'Prehľad známok';
$string['yourmark'] = 'Vaše hodnotenie';
$string['randommark'] = 'Náhodná známka';
$string['passmark'] = 'Potrebná známka';
$string['yourpercentage'] = 'Vaše percentá';
$string['adjusted'] = '(upravené)';
$string['msg1'] = 'Ďakujeme Vám za vyplnenie <strong>%s</strong>. Vaše odpovede boli zaznamenané.';
$string['msg2'] = 'Ak sa chcete odhlásiť, prosím kliknite na \'Zatvoriť okno\' a stlačiť &lt;CTRL&gt; &lt;ALT&gt; a &lt;DELETE&gt; a tým sa z pracovného počítača odhlásiť.';
$string['closewindow'] = 'Zatvoriť okno';
$string['overallcorrectorder'] = 'Celkové správne riešenie (Bonusové body)';
$string['outof'] = 'z';
$string['learningobjectives'] = 'Vzdelávacie ciele';
$string['experimentalquestion'] = '0 - Experimentálna úloha';
$string['unmarked'] = 'Neznámkované';
$string['tdiagnosis'] = 'Diagnóza';
$string['tinvestigation'] = 'Vyšetrenie';
$string['tprescription'] = 'Preskripcia';
$string['tintervention'] = 'Zákrok';
$string['ttreatment'] = 'Liečba';
$string['thankyou'] = 'Ďakujeme';
$string['difficultyofthequestion'] = 'Obtiažnosť úlohy (t.j. nastavené normy). Pre zobrazenie celého názvu kategórie prejdite cez neho ukazovateľom myši.';
$string['withatoleranceof'] = 's toleranciou';
$string['true'] = 'Pravda';
$string['false'] = 'Nepravda';
$string['yes'] = 'Ano';
$string['no'] = 'Nie';
$string['abstain'] = 'Zdržalo sa';
$string['iscorrect'] = 'Je správne';
$string['isexcluded'] = 'Je vyňaté';
$string['withinshape'] = 'V rámci tvaru';
$string['outsideshape'] = 'Mimo tvar';
$string['useranswererror'] = 'Chyba v odpovedi študenta';
$string['errorkeywordunique'] = 'CHYBA: nie je možné nájsť unikátnu úlohu ku kľúčovému slovu';
$string['errorrandomnotfound'] = 'CHYBA: žiadne náhodne vybrané úlohy. Možno bola táto obrazovka preskočená';
$string['question'] = 'Úloha';
$string['feedback'] = 'Komentár';
$string['overriddenby'] = 'Hodnotenie upravené (kým)';
$string['questionclarification'] = 'Objasnenie úlohy';
$string['EnhancedCalcCorrectError'] = 'CHYBA: Správna odpoveď nemohla býť vypočítaná';
$string['student'] = 'Študent';
$string['started'] = 'Začaté';
$string['finished'] = 'Dokončené';
$string['comments'] = 'Komentáre:';
?>