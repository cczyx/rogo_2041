<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_options.inc';

$string['showretired'] = 'Zobraziť neplatné';
$string['screen'] = 'Obrazovka';
$string['screens'] = 'Obrazovky';
$string['mins'] = 'minút';
$string['SelfenrolURL'] = "URL pre samoprihlásenie";
$string['createnewpaper'] = 'Vytvoriť nový dokument';
$string['manageobjectives'] = 'Správa cieľov';
$string['managekeywords'] = 'Správa kľúčových slov';
$string['referencematerial'] = 'Referenčný materiál';
$string['type'] = 'Typ';
$string['author'] = 'Autor';
$string['create'] = 'Vytvoriť';
$string['nomodulesset'] = 'Nie je nastavený žiadny modul';
$string['unspecifiedsession'] = 'Nešpecifikované sedenie';
$string['examcalendar'] = 'Kalendár sumatívneho testovania';
$string['newpaper'] = 'Nový dokument';
?>