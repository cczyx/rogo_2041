<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_types.inc';
require_once '../lang/' . $language . '/paper/new_paper1.php';
require_once '../lang/' . $language . '/include/months.inc';

$string['availability'] = 'Dostupnosť';
$string['summativeexamdetails'] = 'Detaily sumatívnej skúšky';
$string['academicsession'] = 'Ak.rok';
$string['timezone'] = 'Časová zóna';
$string['from'] = 'Od';
$string['to'] = 'Do';
$string['modules'] = 'Modul(y)';
$string['finish'] = 'Dokončiť';
$string['msg4'] = 'Nie sú vybrané žiadne moduly. Dokument musí byť priradený najmenej k jednému modulu.';
$string['msg5'] = "Názov '%s' je už používaný. Vyberte, prosím, iný.";
$string['msg6'] = 'Jedná sa o uzavrenú skúšku, teda bez použitia pomôcok. Študenti nesmú používať iné zdroje (vrátane suseda), než ktoré sú pužité v dokumente. Nie je možné používať žiadne iné elektronické zariadenie, než počítač určený ku skúške. Slovníky <em>nie sú</em> povolené s jedinnou výnimkou. Tí, ktorí <em>nemajú</em> slovenčinu ako rodný jazyk, môžu používať pre preklad do slovenčiny slovník. Za podmienky, že ani jeden z jazykov nie je predmetom skúšky. Odborné slovníky sú zakázané. Počas skúšky a ani po jej ukončení nie je dovolené z učebne odnášať akékoľvek dokumenty a poznámky. Akékoľvek poznámky, ktoré si zapíšete v priebehu skúšky budú zhromaždené dozorujúcim personálom a zlikvidované.';
$string['barriersneeded'] = 'Nutné obmedzenia';
$string['daterequired'] = 'Dátum je povinný';
$string['cohortsize'] = 'Veľkosť skupiny';
$string['wholecohort'] = 'Celá skupina';
$string['sittings'] = 'Smena';
$string['campus'] = 'Kampus';
$string['notes'] = 'Poznámky';
$string['mins'] = 'minút';
$string['hrs'] = 'hodín';
$string['msg7'] = 'Varovanie: Je potrebné vyplniť dátum konania skúšky.';
$string['msg8'] = 'Varovanie: Je potrebné vyplniť dľžku trvania skúšky v minútach.';
$string['msg9'] = 'Varovanie: Je potrebné vyplniť veľkosť skupiny študentov.';
$string['msg10'] = 'WARNING: A paper cannot finish before it starts';
$string['duration'] = 'Trvanie';
$string['na'] = 'N/A';
?>