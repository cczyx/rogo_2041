<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['copypaper'] = 'Kopírovať dokument';
$string['warning'] = 'Varovanie';
$string['titlewarning'] = 'Varovanie ohľadom nadpisu';
$string['nameused'] = 'Názov \'<strong>%s</strong>\' je už používaný.<br />Vyberte, prosím, iný.';
$string['back'] = '&lt; Späť';
$string['copyerror'] = 'Číslo úlohy $q_no) Chyba pri kopírovaní (Úloha) Súbor <strong>\'%s\'</strong> nie je možné kopírovať.';
$string['completemsg'] = 'Váš dokument a úlohy boli skopírované, ale následujúci obrázok skopírovaný nebol.';
$string['filecopywarning'] = 'Varovanie ohľadom kopírovania súboru';
$string['calculation_link_update_error'] = 'K prelinkovaným výpočtovým úlohám sa nepodarilo pridať linky. - <strong>\'%s\'</strong>';
?>