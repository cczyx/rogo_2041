<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['papersearch'] = 'Hľadať v dokumente';
$string['wordorphrase'] = 'Slovo alebo fráza';
$string['formative'] = 'Cvičné sebahodnotenie';
$string['progresstest'] = 'Priebežný test';
$string['summative'] = 'Sumatívna skúška';
$string['survey'] = 'Prieskum';
$string['oscestation'] = 'Stanica OSCE';
$string['offlinepaper'] = 'Offline dokument';
$string['peerreview'] = 'Recenzia';
$string['accessibility'] = 'Dostupnosť';
$string['date'] = 'Dátum';
$string['lab'] = 'Učebňa';
$string['anylab'] = '(ľubovoľná učebňa)';
$string['ownership'] = 'Vlastníctvo';
$string['owner'] = 'Vlastník';
$string['anyowner'] = '(ľubovoľný vlastník)';
$string['papers'] = 'Dokumenty';
$string['back'] = 'Späť';
$string['search'] = 'Hľadať';
$string['nothingfound'] = 'Pri hľadaní v dokumentoch nebolo nič nájdené';
$string['screen'] = 'Obrazovka';
$string['screens'] = 'Obrazovky';
$string['anymodule'] = '(ľubovoľný modul)';
$string['mypaperssonly'] = '(iba moje dokumenty)';
$string['type'] = 'Typ';
$string['author'] = 'Autor';
$string['to'] = 'do';
$string['module'] = 'Modul:';
$string['notonmodules'] = 'No results';
$string['notonmodulesmessage'] = 'You are not in any module teams. No results can be returned.';
?>