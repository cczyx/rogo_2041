<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['internalreport'] = 'Prehľad poznámok interných skúšajúcich';
$string['externalreport'] = 'Prehľad poznámok externých skúšajúcich';
$string['internalcomments'] = 'Interné poznámky Úl.';
$string['externalcomments'] = 'Externé poznámky Úl.';
$string['editquestion'] = 'Upraviť úlohu';
$string['reviewer'] = 'Recenzent';
$string['comment'] = 'Komentár';
$string['action'] = 'Akcia';
$string['response'] = 'Odpoveď';
$string['nocomment'] = 'Bez komentára';
$string['na'] = 'N/A';
$string['Read - disagree'] = 'Čítal - nesúhlasím';
$string['Read - actioned'] = 'Čítal - aktivované';
$string['Not actioned'] = 'NEaktivované';
$string['notreviewed'] = 'NErecenzované';
$string['noresponse'] = 'Bez odpovede';
$string['noreviewers'] = '<strong>Varovanie:</strong> Tento dokument nebol recenzovaný.';
$string['screen'] = 'Obrazovka';
$string['papernotfound'] = 'Dokument nebol nájdený';
$string['furtherassistance'] = 'V prípade potreby ďalšej pomoci, prosím, kontaktujte: <a href="mailto:%s">%s</a>';
$string['reviewers'] = 'Rezenzenti';
$string['started'] = 'Začalo';
$string['completed'] = 'Dokončilo';
$string['generalpapercomments'] = 'Všeobecný komentár k testu';
$string['cannotcomment'] = 'Nie je možné komentovať';
?>