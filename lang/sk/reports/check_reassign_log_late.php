<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['latesubmission'] = 'Neskoro podané';
$string['question'] = 'Úloha';
$string['screen'] = 'Obrazovka';
$string['saved'] = 'Uložené';
$string['ipaddress'] = 'IP Addresa';
$string['reject'] = 'Odmietnuť';
$string['accept'] = 'Prijať';
$string['warning'] = 'Varovanie';
$string['Reason'] = 'Dôvod';
$string['Cancel'] = 'Zrušiť';
$string['msg1'] = '(uveďte dôvody, prečo sú záznamy prijaté či odmietnuté)';
$string['msg2'] = 'Skripty skúšky nemôžu byť pridelené v priebehu skúšky.<br />Počkajte, prosím, na dokončenie skúšky';
$string['msg3'] = 'Ste si naozaj istý/á, že chcete prijať tieto neskoro podané odpovede?';
$string['msg4'] = 'Ste si naozaj istý/á, že chcete odmietnuť a vyradiť tieto odpovede?';
?>