<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['classtotals'] = 'Celkové výkazy triedy';
$string['studentsonly'] ='len študenti';
$string['paper'] = 'Dokument';
$string['temporaryaccountswarning'] = '<strong>Varovanie k dočasným účtom</strong>&nbsp;&nbsp;&nbsp; Priraďte ich, prosím, k príslušným študentských účtom.';
$string['moredetails'] = 'Kliknutím zobrazíte detaily.';
$string['latesubmissionsmsg'] = '<strong>Neskoro zadané odpovede</strong>&nbsp;&nbsp;&nbsp;Používateľom sa údaje uložia po skončení skúšky ';
$string['candidatenotcomplete'] = 'kandidáti nedokončili všetky okna';
$string['cohortsize'] = 'Veľkosť skupiny';
$string['nocompletions'] = 'Nedokončené';
$string['candidateabsent'] = 'chýbajúci kandidát';
$string['candidatesabsent'] = 'chybajúcí kandidáti';
$string['failureno'] = '# Neprospelo';
$string['passno'] = '# Prospelo';
$string['distinctionno'] = '# S vyznamenaním';
$string['totalmarks'] = 'Max. dosiahnuteľná známka';
$string['passmark'] = 'Potrebná známka';
$string['randommark'] = 'Náhodná známka';
$string['ss'] = 'SS'; 
$string['ssdistinction'] = 'SS S vyznamenaním';
$string['meanmark'] = 'Priemerná známka';
$string['medianmark'] = 'Stredová známka';
$string['stdevmark'] = 'Odchýlka známok';
$string['maxmark'] = 'Maximálna známka';
$string['minmark'] = 'Minimálna známka';
$string['range'] = 'Rozsah rozdielov';
$string['top10'] = 'Najlepších 10%';
$string['top15'] = 'Najlepších 15%';
$string['top20'] = 'Najlepších 20%';
$string['top25'] = 'Najlepších 25%';
$string['bottom10'] = 'Najhorších 10%';
$string['averagetime'] = 'Priemerný čas';
$string['excludedquestions'] = 'Vylúčené úlohy';
$string['warnings'] = 'Varovanie';
$string['nolongerappear'] = 'Nájdené odpovede úloh, ktoré nie sú v dokumente (IDs:';
$string['emailssent'] = 'E-maily odoslané';
$string['emailclassmarks'] = 'Zaslať známky';
$string['title'] = 'Titul';
$string['surname'] = 'Priezvisko';
$string['firstnames'] = 'Meno';
$string['studentid'] = 'ID študenta';
$string['username'] = 'Používateľské meno';
$string['course'] = 'Kurz';
$string['module'] = 'Modul';
$string['mark'] = 'Známka';
$string['classification'] = 'Klasifikácia';
$string['starttime'] = 'Začaté';
$string['duration'] = 'Trvanie';
$string['ipaddress'] = 'IP Adresa';
$string['%'] = '%'; 
$string['adjusted%'] = 'Upravené v %';
$string['room'] = 'Miestnosť';
$string['fail'] = 'Neuspel/a';
$string['pass'] = 'Uspel/a';
$string['distinction'] = 'S vyznamenaním';
$string['papernotes'] = 'Poznámky k testu';
$string['distributionchart'] = 'Grafické rozloženie dosiahnutých %';
$string['scatterplot'] = 'Grafické rozloženie skutočnej dĺžky trvania';
$string['summary'] = 'Prehľad';
$string['examscript'] = 'Skript skúšky';
$string['feedback'] = 'Komentár';
$string['studentprofile'] = 'Profil študenta';
$string['resettimer'] = 'Vynulovať stopky';
$string['newnote'] = 'Nová poznámka...';
$string['reassigntouser'] = 'Novo priradiť k používateľovi...';
$string['latesubmissions'] = 'Neskoro podané';
$string['na'] = 'N/A';
$string['noattempts'] = 'V danom období <strong>%s &ndash; %s</strong> skúšku neurobil žiadny študent.';
$string['marks'] = 'Hodnotenie';
$string['studentssubmitted'] = 'Odpovede študentov';
$string['noattendance'] = 'Neprítomné';
$string['alternativearrangements'] = 'Náhradné opatrenia';
$string['displayexamscript'] = 'Zobraz skript skúšky';
$string['displaysurvey'] = 'Zobraziť prieskum';
$string['displaypaper'] = 'Zobraziť dokument';
$string['notcompleted'] = 'Varovanie: všetky okná nie sú vyplnené';
$string['unknown'] = '&nbsp;&lt;Neznáme&gt;';
$string['markingnotcomplete'] = 'Známkovanie nie je kompletné';
$string['of'] = ' zo ';
$string['percentofcohort'] = '% zo skupiny';
$string['couldnotsend'] = 'Odoslanie zlyhalo u týchto používateľov';
$string['top'] = 'Najlepší';
$string['bottom'] = 'Najhorší';
$string['rank'] = 'Poradie';
$string['unmarkedenhancedcalc'] = '<strong>Neohodnotené úlohy</strong>&nbsp;&nbsp;&nbsp;Některé <em>výpočtové</em> úlohy neboli doteraz oznámkované.';
$string['unmarkedtextbox'] = '<strong>Neohodnotené úlohy</strong>&nbsp;&nbsp;&nbsp;Niektoré <em>textové<em> úlohy neboli doteraz oznámkované.';
$string['midexamclarifications'] = 'Objasnenie v priebehu skúšky';
$string['skippedquestions'] = 'Vynechané úlohy';
$string['decile'] = 'Decil';
$string['hostnames'] = 'Dočasné účty';
$string['deciles'] = 'Decily';
$string['quartiles'] = "Kvartily";
$string['markingcalcquestions'] = 'Hodnotenie výpočtových úloh';
$string['marking'] = 'Hodnotenie...';

$string['viewstudentnote'] = 'Zobraziť poznámku študenta...';
$string['viewaccessibility'] = 'Zobraziť nastavenie prístupnosti...';
$string['resitcandidate'] = 'Opakovanie (reparát)';
$string['publishmarks'] = 'Publish Marks';
$string['gradepublish'] = 'Marks Published';
?>