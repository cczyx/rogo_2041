<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['remark'] = 'Znova oznámkovať';
$string['msg'] = 'Pri prepisovaní používateľovej známky, vyberte, prosím, či si prajete k úlohe priradiť úplnú, čiastočnú, alebo žiadnu známku a vložte dôvod zmeny.';
$string['useranswers'] = 'Odpovede používateľa';
$string['variables'] = 'Premenné';
$string['answers'] = 'Odpovede';
$string['useranswer'] = 'Používateľ';
$string['units'] = 'Jednotky';
$string['correctans'] = 'Správne';
$string['distance'] = 'Vzdialenosť';
$string['marks'] = 'Hodnotenie';
$string['fullmarks'] = 'Celková známka';
$string['partialmarks'] = 'Čiastočná známka';
$string['incorrect'] = 'Nesprávne';
$string['reason'] = 'Dôvod';
$string['na'] = 'N/A';
$string['addreason'] = 'Pridať dôvod';
$string['nomarkmsg'] = 'Nebola vybraná žiadna známka k prepísaniu';
$string['saveerror'] = 'Pri ukladaní známky sa vyskytla chyba. Skúste, prosím, znova';
$string['done'] = 'Hotovo';
?>