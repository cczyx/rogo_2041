<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['marks'] = 'Hodnotenie';
$string['saveexit'] = 'Uložiť &amp; ukončiť';
$string['savecontinue'] = 'Uložiť &amp; pokračovať';
$string['noanswer'] = 'Bez odpovede';
$string['textboxmarking'] = 'Hodnotenie textboxu';
$string['nostudents'] = 'Žiadni študenti';
$string['saveerror'] = 'Pri ukladaní známky sa vyskytla chyba. Skúste, prosím, znova';
$string['hidemarked'] = 'Skryť oznámkované';
$string['primarymarking'] = 'Prvotné hodnotenie';
$string['secondmarking'] = 'Druhotné hodnotenie';
$string['finalisemarks'] = 'Dokončiť hodnotenie';
$string['candidates'] = 'Kandidáti';
$string['screen'] = 'Obrazovka';
$string['note'] = 'Poznámka';
$string['comments'] = 'Komentáre';
$string['next'] = 'Ďaľší &gt;';
$string['previous'] = '&lt; Predchádzajúci';
$string['finish'] = 'Dokončiť';
$string['mark_progress'] = 'Študent %d z %d';
$string['answer_saved'] = 'Známky uložené';
$string['noattempts'] = 'V danom období <strong>%s &ndash; %s</strong> skúšku neurobil žiadny študent.';
$string['tooltip_comments'] = 'Komentáre sú len pre zamestnancov. Tieto nemôžu byť prezerané študentmi.';
?>