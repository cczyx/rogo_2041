<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['frequencydiscrimination'] = 'Frekvenčná & diskriminačná analýza';
$string['reporttitle'] = 'Frekvenčná &amp; diskriminačná (U-L) analýza';
$string['PaperNotAttempted'] = "Tento test sa nikdo nepokúsil vyplniť.";
$string['NotEnoughData'] = "Nedostatok údajov k výpočtu najlepšej a najhoršej skupiny. Zvoľte vyššie percento, prosím.";
$string['AllItemsCorrect'] = "Všetky položky správne";
$string['totalcandidatenumber'] = 'Celkový počet kandidátov';
$string['FullMarks'] = "Celkové známky";
$string['PartialMarks'] = "Čiastočné známky";
$string['Incorrect'] = "Nesprávne";
$string['Correct'] = "Správne";
$string['True'] = "Pravdivý";
$string['False'] = "Nepravdivý";
$string['TopGroup'] = "Najlepšia skupina";
$string['BottomGroup'] = "Najhoršia skupina";
$string['meanWordCount'] = "priemer počtu slov";
$string['groupsizes'] = 'Horná/spodná veľkosť skupiny';
$string['pergroup'] = 'za skupinu';
$string['boldstems'] = 'Tučne označené';
$string['correctanswers'] = 'reprezentuje správne odpovede';
$string['p_definition'] = 'Obtiažnosť položky (podiel správnych odpovedí študentov)';
$string['d_definition'] = 'hodnota citlivosti';
$string['t_definition'] = 'percento zo <strong>všetkých</strong> položiek zodpovedaných skupinou';
$string['u_definition'] = 'percento z <strong>horných</strong> položiek zodpovedaných skupinou';
$string['l_definition'] = 'percento zo <strong>spodných</strong> položiek zodpovedaných skupinou';
$string['warning'] = 'Varovanie';
$string['p_warning'] = '<strong>p < 0.2</strong> (t.j. veľmi ťažké)';
$string['d_warning'] = "<strong>d < 0.15</strong> (t.j. slabá)<br />Zistená signalizácia možného nedostatku, ak usúdite, že je položka zlá, vylúčite ju z používania pomocou ikony <img src=\"../artwork/exclude_off.gif\" style=\"cursor:pointer\" width=\"23\" height=\"22\" border=\"0\" alt=\"Vylúčiť\" /> a kliknete na  'Uložiť' v spodnej časti okna";
$string['summary'] = 'Prehľad';
$string['msg'] = 'Počet položiek môže byť väčší než počet úloh, keďže, napríklad, dichotomické úlohy, popisky a úlohy s výberom odpovede, sú tvorené z viacerých položiek a každá z nich môže mať vlastné hodnoty citlivosti a náročnosti.';
$string['difficulty'] = 'Náročnosť';
$string['discrimination'] = 'Citlivosť';
$string['noofitems'] = 'Počet položiek';
$string['veryeasy'] = 'Veľmi ľahká';
$string['easy'] = 'Jednoduchá';
$string['moderate'] = 'Priemerná';
$string['hard'] = 'Neľahká';
$string['veryhard'] = 'Veľmi ťažká';
$string['mean'] = 'Priemer';
$string['highest'] = 'Najvyššie';
$string['high'] = 'Vysoké';
$string['intermediate'] = 'Stredné';
$string['low'] = 'Nízke';
$string['save'] = 'Uložiť výnimky';
$string['screen'] = 'Obrazovka';
$string['warning1'] = 'Varovanie: Náročnosť je menšia ako 0.2';
$string['warning2'] = 'Varovanie: Rozlíšenie je menšie ako 0.15';
$string['abstain'] = 'Zdržalo sa';
$string['unmarkedscripts'] = '%d neohodnotených skript';
$string['randomwarning'] = '<strong>Varovanie:</strong> Otázky typu Blok náhodných úloh nemôžu byť analyzované v tejto zostave.';
$string['keywordwarning'] = '<strong>Varovanie:</strong> Otázky typu Kľúčových slov nemôžu byť analyzované v tejto zostave.';
$string['paperpublishedwarning'] = '<strong>Paper grades have been published.</strong>&nbsp;&nbsp;&nbsp;Marking adjustment can no longer occur.';
$string['paperlockedclick'] ='Click for more details';
?>