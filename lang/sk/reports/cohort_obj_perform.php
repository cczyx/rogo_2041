<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['studentsonly'] = 'len študenti';
$string['learningobjectiveanalysis'] = 'Analýza vzdelávacích cieľov';
$string['msg1'] = 'Túto skúšku neurobil žiaden študent';
$string['msg2'] = 'Tento dokument nebol namapovaný k žiadnemu vzdelávaciemu cieľu.';
$string['learningobjectives'] = 'Vzdelávacie ciele';
$string['totalcandidate'] = 'Celkom kandidátov #';
$string['key1'] = 'Získanie 80-100% zo špecifického cieľa';
$string['key2'] = 'Získanie 50-79% zo špecifického cieľa';
$string['key3'] = 'Získanie 0-49% zo špecifického cieľa';
$string['key4'] = '<a href="" onclick="return false;">odkaz</a> - skok do sekcie VLE, kde získate ďalšie podrobnosti';
$string['msg'] = 'Nižšie je uvedený zoznam %d unikátnych cieľov, ktoré sú týmto dokumentom posudzované. Pretože niektoré ciele môžu byť testované viacerými úlohami, je možné získať čiastočné splnenie cieľa. Následujúcí zoznam je zoradený podľa percentuálneho splnenia každého cieľa v skupine - začíne najvyšším.';
$string['uppersize'] = 'Horná veľkosť skupiny';
$string['lowersize'] = 'Spodná veľkosť skupiny';
$string['candidates'] = 'Kandidáti';
$string['completely'] = 'Plne/Väčšinou získané';
$string['partically'] = 'Čiastočne získané';
$string['mostly'] = 'Väčšinou nezískané';
$string['shortcut'] = 'Skratka';
?>