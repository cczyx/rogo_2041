<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['Reassign Script to User'] = 'Priradiť skripty k používateľovi';
$string['search'] = 'Hľadať';
$string['warning'] = 'Upozornenie';
$string['msg2'] = 'Skripty skúšky nemôžu byť pridelené v priebehu skúšky.<br />Počkajte, prosím, na dokončenie skúšky';
$string['msg3'] = 'bol vyhradený s následujúcimi údajmi';
$string['Title'] = 'Titul';
$string['Last Name'] = 'Priezvisko';
$string['First Names'] = 'Meno';
$string['Student ID'] = 'ID študenta';
$string['msg4'] = 'Nenájdený žiadny používateľ s odpovedajúcimi údajmi.';
$string['Reassign answers'] = 'Priradiť odpovede/známky';
$string['to following user'] = 'následujúcemu používateľovi';
$string['user_not_on_paper_modules'] = 'The student is not on any of the paper\'s modules.';
?>
