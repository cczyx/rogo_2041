<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/months.inc';

$string['summativeexamdetails'] = 'Detaily sumatívnej skúšky';
$string['summativescheduling'] = 'Plánovánie sumatívnej skúšky';
$string['jumptopaper'] = 'Prejsť na dokument';
$string['editproperties'] = 'Upraviť vlastnosti';
$string['converttoformative'] = 'Konvertovať na cvičný test';
$string['Paper'] = 'Dokument';
$string['papername'] = 'Názov dokumentu';
$string['paperowner'] = 'Vlastník dokumentu';
$string['session'] = 'Relácia';
$string['modules'] = 'Modul/y';
$string['examduration'] = 'Trvanie skúšky';
$string['cohortsize'] = 'Veľkosť skupiny';
$string['whole cohort'] = '&lt;celá skupina&gt;';
$string['sittings'] = 'Smena';
$string['examperiod'] = 'Platnosť skúšky';
$string['barriersneeded'] = 'Nutné obmedzenie';
$string['Yes'] = 'Áno';
$string['No'] = 'Nie';
$string['campus'] = 'Kampus';
$string['notes'] = 'Poznámky';
$string['examtasks'] = 'Úlohy ku skúške';
$string['Email'] = 'E-mail';
?>