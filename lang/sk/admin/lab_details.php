<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['labdetails'] = 'Detaily učebne';
$string['computerlabs'] = 'Počítačová učebňa';
$string['createnewlab'] = 'Nová učebňa';
$string['vieweditdetails'] = 'Zobraziť/Upraviť detaily';
$string['deletelab'] = 'Odstrániť učebňu';
$string['lowbandwidth'] = 'Pomalé pripojenie';
$string['name'] = 'Názov';
$string['campus'] = 'Kampus';
$string['building'] = 'Budova';
$string['roomnumber'] = 'Číslo miestnosti';
$string['bandwidth'] = 'Pripojenie';
$string['low'] = 'Pomalé';
$string['high'] = 'Rýchle';
$string['timetabling'] = 'Rozvrh';
$string['itsupport'] = 'IT podpora';
$string['plagarism'] = 'Anti plagiátorská nóta';
$string['ipaddresses'] = 'IP adresy';
$string['edit'] = 'Upraviť';
$string['listcampuses'] = 'Campuses';
?>