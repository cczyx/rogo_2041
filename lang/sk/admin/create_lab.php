<?php           //cz
$string['machine'] = 'Počítač';
$string['machines'] = 'Počítače';
$string['createnewlab'] = 'Nová učebňa';
$string['vieweditdetails'] = 'Zobraziť/Upraviť detaily';
$string['deletelab'] = 'Odstrániť učebňu';
$string['lowbandwidth'] = 'Pomalé pripojenie';
$string['name'] = 'Názov';
$string['campus'] = 'Kampus';
$string['building'] = 'Budova';
$string['roomnumber'] = 'Číslo miestnosti';
$string['bandwidth'] = 'Pripojenie';
$string['low'] = 'Pomalé';
$string['high'] = 'Rýchle';
$string['timetabling'] = 'Rozvrh';
$string['itsupport'] = 'IT podpora';
$string['plagarism'] = 'Anti plagiátorská metóda';
$string['ipaddresses'] = 'IP adresy';
$string['save'] = 'Uložiť';
$string['badaddressesmsg'] = 'Následujúca IP adresa bola chybná a nebola zaradená do učebne: %s. Ostatné adresy boli uložené.';
$string['backtolabs'] = 'Späť na zoznam učební';
$string['noipaddresses'] = 'Učebňu nie je možné vytvoriť, nie sú uvedené žiadne IP adresy.';
?>