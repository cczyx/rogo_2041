<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['systeminformation'] = 'Systémové informácie';
$string['table'] = 'Tabuľka';
$string['records'] = 'Záznamy';
$string['updated'] = 'Aktualizované';
$string['application'] = 'Rogō aplikácia';
$string['version'] = 'Verzia';
$string['webroot'] = 'Web Root';
$string['database'] = 'Databáza';
$string['authentication'] = 'Autentizácia';
$string['serverinformation'] = 'Informácie o serveri';
$string['processor'] = 'Procesor';
$string['cpus'] = 'CPUs';
$string['cores'] = 'Jadra';
$string['servername'] = 'Názov servera';
$string['hostname'] = 'Názov hosta'; 
$string['ipaddress'] = 'IP addresa';
$string['clock'] = 'Čas';
$string['os'] = 'OS';
$string['apache'] = 'Apache'; 
$string['php'] = 'PHP';
$string['mysql'] = 'MySQL'; 
$string['clientcomputer'] = 'Klientský počítač';
$string['browser'] = 'Prehliadač';
$string['partitions'] = 'Rozdelenie';
$string['mysqlstatus'] = 'MySQL Status';
$string['uptime'] = 'Doba prevádzky';
$string['threads'] = 'Vlákna';
$string['questions'] = 'Úlohy';
$string['slow queries'] = 'Pomalé dotazy'; 
$string['opens'] = 'Opens'; 
$string['flush tables'] = 'Flush tables'; 
$string['open tables'] = 'Open tables'; 
$string['queries per second avg'] = 'Queries per second avg'; 
$string['na'] = 'N/A';
$string['driveicon'] = 'Drive icon'; 
$string['freespace'] = '%s voľných z %s';
$string['More details'] = 'Ďalšie detaily...';
$string['lookups'] = 'Vyhľadávanie'; 
$string['interactivequestions'] = 'Interaktívne úlohy';
$string['minutes'] = 'minúty';
$string['hours'] = 'hodiny';
$string['days'] = 'dni';
$string['Session'] = 'Relácie';
$string['rogoplugins'] = 'Rogō plug-iny';
$string['EnhancedCalcPlugin'] = 'Početná úloha';
$string['ErrorLogSettings'] = 'Hlásenie chýb';
$string['authdebug'] = 'Zobraziť overovací debug';
$string['errorsonscreen'] = 'Zobraziť chyby na obrazovke';
$string['phpnotices'] = 'Zobraziť PHP oznamy';
$string['errorshutdown'] = 'Riešenie chýb pri výpadku';
$string['varcapturemethod'] = 'Rôzne metódy zachovania údajov pri výpadku:';
$string['improved'] = 'Vylepšená';	
$string['basic'] = 'Základná';	
$string['none'] = 'Žiadna';
$string['company'] = 'Spoločnosť';
$string['webserver'] = 'Web server';
$string['errorslogged'] = 'Errors logged to file';
?>
