<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/months.inc';

$string['allschools'] = '&lt;Všetky školy&gt;';
$string['alllabs'] = '&lt;Všetky učebne&gt;';
$string['default'] = 'Predvolené';
$string['calendar'] = 'Kalendár';
$string['calendaryears'] = 'Kalendárne roky';
$string['monday'] = 'Pondelok';
$string['tuesday'] = 'Utorok';
$string['wednesday'] = 'Streda';
$string['thursday'] = 'Štvrtok';
$string['friday'] = 'Piatok';
$string['saturday'] = 'Sobota';
$string['starttime'] = 'Čas začiatku';
$string['endtime'] = 'Čas ukončenia';
$string['duration'] = 'Trvanie';
$string['timezone'] = 'Časová zóna';
$string['time_warning'] = 'Čas začiatku a čas ukončenia je totožný!';
$string['lab_warning'] = 'Upozornenie: nie je nastavená žiadna počítačová učebňa!';
$string['duration_warning'] = 'Nie je nastavená dĺžka trvania!';
$string['labs'] = 'Počítačové učebne';
$string['metadata'] = 'Metadata';
$string['extraevents'] = '<strong>SysAdmin:</strong> Pre pridanie udalosti dvakrát kliknite na kalendár.';
?>