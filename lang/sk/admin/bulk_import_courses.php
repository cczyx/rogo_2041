<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['entermoduleid'] = 'Zadajte, prosím, identifikátor modulu.';
$string['entermoduletitle'] = 'Zadajte, prosím, nadpis modulu.';
$string['moduleid'] = 'ID modulu';
$string['createcourse'] = 'Nový kurz';
$string['bulkcourseimport'] = 'Nahrať kurz';
$string['editcourse'] = 'Upraviť kurz';
$string['deletecourse'] = 'Odstrániť kurz';
$string['msg1'] = 'Táto funkcia umožňuje nahrať kurzy hromadne. CSV súbor by mal obsahovať následujúce polia:';
$string['csvfile'] = 'CSV súbor:';
$string['import'] = 'Importovať';
$string['alreadyexists'] = 'Už existuje';
$string['added'] = 'Pridané';
$string['failed'] = 'Nepodarilo sa';
?>