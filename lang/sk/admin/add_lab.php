<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['machine'] = 'Počítač';
$string['machines'] = 'Počítače';
$string['createnewlab'] = 'Nová učebňa';
$string['computerlabs'] = 'Počítačové učebne';
$string['vieweditdetails'] = 'Zobraziť/Upraviť detaily';
$string['deletelab'] = 'Odstrániť učebňu';
$string['lowbandwidth'] = 'Pomalé pripojenie';
$string['name'] = 'Názov';
$string['campus'] = 'Kampus';
$string['building'] = 'Budova';
$string['roomnumber'] = 'Číslo miestnosti';
$string['bandwidth'] = 'Pripojenie';
$string['low'] = 'Pomalé';
$string['high'] = 'Rýchle';
$string['timetabling'] = 'Rozvrh';
$string['itsupport'] = 'IT Podpora';
$string['plagarism'] = 'Plagiátorstvo';
$string['ipaddresses'] = 'IP Adresy';
$string['save'] = 'Uložiť';
$string['badaddressesmsg'] = 'Následujúce IP adresy boli chybné a neboli do učebne zaradené: %s. Ostatné adresy boli uložené.';
$string['backtolabs'] = 'Späť na zoznam učební';
$string['noipaddresses'] = 'Učebňu nie je možné vytvoriť, nie sú uvedené žiadne IP adresy.';
$string['nolabname'] = 'Bezmennú učebňu nie je možné vytvoriť.';
$string['listcampuses'] = 'Campuses';
?>