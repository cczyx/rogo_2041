<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/admin/list_modules.php';
require '../lang/' . $language . '/admin/add_module.php';

$string['entermoduleid'] = 'Zadajte, prosím, identifikátor modulu.';
$string['entermoduletitle'] = 'Zadajte, prosím, nadpis modulu.';
$string['moduleid'] = 'ID modulu';
$string['msg1'] = 'Táto funkcia umožňuje nahrať moduly hromadne. Študentov však už zapisujte do modulov samostatne.';
$string['msg2'] = "Medzi povinné polia patria iba položky 'ID Modulu', 'Názov' a 'Škola'.";
$string['csvfile'] = 'CSV súbor:';
$string['import'] = 'Importovať';
$string['alreadyexists'] = 'Už existuje';
$string['added'] = 'Pridané';
$string['failed'] = 'Nepodarilo sa';
?>