<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['errordetails'] = 'Chyba #%s detaily';
$string['displaydetails'] = 'Zobraziť detaily';
$string['date'] = 'Dátum';
$string['staff'] = 'Zamestnanec';
$string['type'] = 'Typ';
$string['description'] = 'Popis';
$string['file'] = 'Súbor';
$string['paperid'] = 'ID dokumentu';
$string['querystring'] = '$_SERVER[\'QUERY_STRING\']'; 
$string['post'] = '$_POST'; 
$string['phpself'] = '$_SERVER[\'PHP_SELF\']'; 
$string['requestmethod'] = '$_SERVER[\'REQUEST_METHOD\']';
$string['occurrenceoferror'] = 'Výskyt chyby';
$string['datefixed'] = 'Dátum opravy';
$string['na'] = 'N/A'; 
$string['close'] = 'Uzavrené';
$string['fixed'] = 'Opravené';
$string['username'] = 'Používateľské meno';
$string['backtrace'] = 'Spätné sledovanie';
$string['variables'] = 'Premenné';

?>