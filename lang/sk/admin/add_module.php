<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/admin/list_modules.php';

$string['entermoduleid'] = 'Zadajte, prosím, identifikátor modulu.';
$string['entermoduletitle'] = 'Zadajte, prosím, nadpis modulu.';
$string['selectschool'] = 'Priraďte, prosím, k modulu školu.';
$string['moduleid'] = 'ID modulu';
$string['name'] = 'Názov';
$string['school'] = 'Škola';
$string['smsapi'] = 'SMS API';
$string['academicyearstart'] = 'Začiatok akademického roka';
$string['objapi'] = 'API cieľov';
$string['summativechecklist'] = 'Sumatívny kontrolný zoznam';
$string['peerreview'] = 'Recenzia';
$string['externalexaminers'] = 'Externí recenzenti';
$string['standardssetting'] = 'Nastavenie štandardov';
$string['mapping'] = 'Mapovanie';
$string['active'] = 'Aktívny';
$string['allowselfenrol'] = 'Povoliť samoprihlásenie';
$string['negativemarking'] = 'Záporné známkovanie';
$string['add'] = 'Pridať';
$string['save'] = 'Uložiť';
$string['createmodule'] = 'Nový modul';
$string['bulkmoduleimport'] = 'Nahrať modul';
$string['editmodule'] = 'Upraviť modul';
$string['deletemodule'] = 'Odstrániť modul';
$string['modulefolder'] = 'Priečinok modulu';
$string['studentcohort'] = 'Skupina študentov';
$string['nolookup'] = '&lt;Nenájdené&gt;';
$string['ebelgrid'] = 'Ebelova mriežka';
$string['timedexams'] = 'Skúška s časovým limitom';
$string['questionbasedfeedback'] = 'Komentáre k úlohám';
$string['addteammembers'] = 'Pridať členov do tímu';
$string['session'] = 'Relácie';
$string['module'] = 'Modul';
$string['maplevel'] = 'Úroveň mapovania';
$string['level'] = 'Úroveň';
$string['tooltip_format'] = 'Vo formáte MM/DD';
$string['duplicateerror'] = 'Module ID in use, please use another.';
?>