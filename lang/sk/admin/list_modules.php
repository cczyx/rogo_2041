<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['modules'] = 'Moduly';
$string['moduleid'] = 'ID modulu';
$string['name'] = 'Názov';
$string['school'] = 'Škola';
$string['active'] = 'Aktívne';
$string['yes'] = 'Áno';
$string['no'] = 'Nie';
$string['createmodule'] = 'Nový modul';
$string['bulkmoduleimport'] = 'Nahrať modul';
$string['editmodule'] = 'Upraviť modul';
$string['deletemodule'] = 'Vymazať modul';
$string['modulefolder'] = 'Priečinok modulu';
$string['studentcohort'] = 'Skupina študentov';
$string['currentmodule'] = 'Aktuálny modul';	
$string['moduleimports'] = 'Importy modulu';	
$string['importsummary'] = 'Prehľad k importu';
$string['externalid'] = 'External ID';
?>