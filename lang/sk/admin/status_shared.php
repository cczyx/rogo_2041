<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['questionstatuses'] = 'Stav úlohy';
$string['createstatus'] = 'Nový stav';
$string['edit'] = 'Upraviť';
$string['add'] = 'Pridať';
$string['status'] = 'Stav';
$string['deletestatus'] = 'Smazať Stav';
$string['statuses'] = 'Stavy';
$string['name'] = 'Názov';
$string['excludemarking'] = 'Neznámkovať';
$string['retired'] = 'Neplatná';
$string['default'] = 'Štandardná';
$string['setlocked'] = 'Je možné nastaviť u zamknutých úloh';
$string['validate'] = 'Overiť';
$string['displaywarning'] = 'Zobraziť upozornenie';
$string['colour'] = 'Farba';
$string['enternameofstatus'] = 'Zadajte, prosím, názov stavu.';
$string['duplicateerror'] = 'Názvy stavov sa nesmú zhodovať.';