<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['calendar'] = 'Kalendár';
$string['clearguestaccounts'] = 'Vyčistiť dočasné účty';
$string['clearoldlogs'] = 'Odstrániť staré prístupy';
$string['clearorphanmedia'] = 'Odstrániť osirelé mediálne súbory ';
$string['cleartraining'] = 'Vyčistiť obsah cvičného modulu';
$string['computerlabs'] = 'Počítačové učebne';
$string['courses'] = 'Kurzy';
$string['deniedlogwarnings'] = 'Upozornenie na zakázané prístupy';
$string['ebelgridtemplates'] = 'Šablóny Ebelovej mriežky';
$string['faculties'] = 'Fakulty';
$string['modules'] = 'Moduly';
$string['phpinfo'] = 'phpinfo()';
$string['schools'] ='Školy';
$string['smsimports'] = 'Nahrať SMS ';
$string['statistics'] = 'Štatistika';
$string['systemerrors'] = 'Systémové chyby';
$string['summativescheduling'] = 'Plánovanie sumatívnej skúšky';
$string['systeminformation'] = 'Systémové informácie';
$string['usermanagement'] = 'Nastavenie používateľov';
$string['msg1'] = 'Ste si naozaj istý/á, že chcete odstrániť všetky dokumenty / úlohy z cvičného modulu?';
$string['msg2'] = 'Ste si naozaj istý/á, že chcete odstrániť staré Prístupy k cvičným a priebežným testom?\n\n(Prístupy k sumatívnym skúškam a prieskumom nebudú zmenené)';
$string['announcments'] = 'Novinky &amp; Oznamy';
$string['imslti'] = 'Kľúče LTI';
$string['questionstatuses'] = 'Stav úloh';
$string['savefailattempts'] = 'Uložiť neúspešné pokusy';
$string['testing'] = 'Testovanie systémov';
$string['bug'] = 'Hlásenie chýb';
$string['academicsessions'] = 'Academic Sessions';
$string['authentication'] = 'OAuth Authentication';
$string['plugins'] = 'Plugins';
$string['imssettings'] = 'IMS Settings';
$string['config'] = 'Configuration';

?>