<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['createschool'] = 'Nová škola';
$string['editschool'] = 'Upraviť školu';
$string['deleteschool'] = 'Odstrániť školu';
$string['schools'] ='Školy';
$string['school'] = 'Škola';
$string['name'] = 'Názov';
$string['faculty'] = 'Fakulta';
$string['prompt'] = 'Odbor';
$string['enternameofschool'] = 'Zadajte, prosím, názov školy.';
$string['duplicateerror'] = 'V rámci fakulty nie je možné použiť rovnaký názov dvakrát.';
$string['externalid'] = 'External ID';
$string['externalsys'] = 'External System';
$string['code'] = 'School Code';
?>