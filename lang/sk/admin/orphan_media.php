<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['removeorphanmedia'] = 'Odstrániť osirelé mediálne súbory ';
$string['notremoving'] = 'Neodstraňovať:';
$string['inexamptionslist'] = 'V zozname výnimiek';
$string['deletefailed'] = 'Odstránenie bolo neúspešné:';
$string['removed'] = 'Odstránené:';
$string['toonew'] = 'Príliš nový';
$string['missingfiles'] = 'Chýbajúce súbory!';
$string['cleanupsummary'] = 'Prehľad čistenia';
$string['filedeleted'] = 'Odstránených súborov ';
$string['spacereclaimed'] = 'Uvoľnený priestor';
$string['deletingfiles'] = 'Odstraňovanie súborov!';
$string['delete'] = 'Delete Files';
$string['noorphanedfiles'] = 'No Orphaned Files Found';
$string['toremove'] = 'To Remove:';
?>