<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['detailed_authentication_information']='Podrobná autentizačná správa';
$string['System Information'] = 'Systémová správa';
$string['No'] = 'Číslo';
$string['Name'] = 'Názov';
$string['Class'] = 'Trieda';
$string['Version'] = 'Verzia';
$string['Settings'] = 'Nastavenia';
$string['Function'] = 'Funkcia';
$string['Description'] = 'Popis';
$string['ID'] = 'ID';
?>