<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['systemupdate'] = 'Systémová aktualizácia';
$string['actionrequired'] = 'Požadovaná akcia';
$string['readonly'] = "Nezabudnite nastaviť <strong>/config/config.inc.php</strong> len na čítanie! (chmod 444)";
$string['finished'] = 'Dokončené!';
$string['couldnotwrite'] = 'Chyba: Nie je možné zapísať konfiguračný súbor!';
$string['msg1'] = 'Tento skript aktualizuje databázové štruktúry tak, aby odpovedali novému %s kódu. Nevadí, ak je tento skript spustený viackrát, keďže kontroluje aktuálnu štruktúru databázy pred aplikáciou akýchkoľvek zmien.';
$string['msg2'] = 'Aktualizačný skript potrebuje používateľské meno a heslo MySQL admina k aktualizácii databázy, používateľov a tabuliek. Toto používateľské meno nie je na serveri uložené a je použité iba pre tento aktualizačný skript.';
$string['databaseadminuser'] = 'Databázový administrátor';
$string['dbusername'] = 'DB Používateľské meno';
$string['dbpassword'] = 'DB Heslo';
$string['onlinehelpsystems'] = 'Systém on-line nápovedy';
$string['updatestaffhelp'] = 'Aktualizovať zamestnaneckú nápovedu';
$string['updatestudenthelp'] = 'Aktualizovať študentskú nápovedu';
$string['startupdate'] = 'Spustiť aktualizáciu';
$string['warning1'] = 'Táto aktualizácia vyžaduje, aby /config/config.inc.php bol zapisovateľný.';
$string['warning2'] = 'Prosím zmente vlastníka súboru (chown) na webserver a nastavte chomod 644';
$string['warning3'] = 'Táto aktualizácia vyžaduje, aby /config priečinok bol zapisovateľný.';
$string['warning4'] = 'Prosím zmente vlastníka súboru (chown) na webserver a nastavte chomod 744';
$string['updatefromversion'] = 'Aktualizácia z verzie';
$string['home'] = 'Domov';
$string['startingupdate'] = 'Spustenie aktualizácie';
$string['addinglines'] = 'Pridanie riadkov do súboru %s :';
$string['replacinglines'] = 'Nahradenie riadku v súbore %s :';
?>