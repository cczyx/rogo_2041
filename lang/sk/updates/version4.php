<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['systemupdate'] = 'Aktualizácia systému';
$string['actionrequired'] = 'Požadovaná akcia';
$string['readonly'] = "Nezabudnite sprístupniť <strong>/config/config.inc.php</strong> len na čítanie! (chmod 444)";
$string['finished'] = 'Dokončené!';
$string['couldnotwrite'] = 'Chyba: nie je možné zapísať konfiguračný súbor!';
$string['msg1'] = 'Tento skript aktualizuje databázové štruktúry tak, aby odpovedali novému %s kódu. Nevadí, ak je tento skript spustený opakovane, keďže kontroluje aktuálnu štruktúru databázy pred aplikáciou akýchkoľvek zmien.';
$string['msg2'] = 'Aktualizačný skript potrebuje k aktualizácii databázy používateľov a tabuľky prihlasovacie meno a heslo administrátora MySQL. Toto používateľské meno nie je na serveri uložené a je použité len týmto aktualizačným skriptom.';
$string['databaseadminuser'] = 'Administrátor databázy';
$string['dbusername'] = 'DB používateľské meno';
$string['dbpassword'] = 'DB heslo';
$string['onlinehelpsystems'] = 'Systém on-line nápovedy';
$string['updatestaffhelp'] = 'Aktualizácia nápovedy pre zamestnancov';
$string['updatestudenthelp'] = 'Aktualizácia nápovedy pre študentov';
$string['startupdate'] = 'Spustiť aktualizáciu';
$string['warning1'] = 'Táto aktualizácia vyžaduje možnosť zápisu do /config/config.inc.php.';
$string['warning2'] = 'Prosím zmente vlastníka súboru (chown) na webserver a nastavte chomod 644';
$string['warning3'] = 'Táto aktualizácia vyžaduje možnosť zápisu do priečinka /config .';
$string['warning4'] = 'Prosím zmente vlastníka súboru (chown) na webserver a nastavte chomod 744';
$string['updatefromversion'] = 'Aktualizovať verziu';
$string['home'] = 'Domov';
$string['startingupdate'] = 'Spustiť aktualizáciu';
?>