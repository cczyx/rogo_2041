<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['msg1'] = 'Vložte <b>krátky</b> popis, ktorý bude pri aktualizácii názvu súboru, pripojený';
$string['tag'] = 'Popis';
$string['create'] = 'Vytvoriť';
$string['success'] = 'Aktualizovať súbor %s vytvorený';
$string['createerror'] = 'Chyba pri tvorbe aktualizovaného súboru.';
$string['runupdate'] = 'Spustiť aktualizáciu';
?>