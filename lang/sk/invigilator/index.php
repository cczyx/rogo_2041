<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['invigilatoraccess'] = 'Prístup dozorujúceho';
$string['lab'] = 'Učebňa:';
$string['unknownlab'] = ' - neznáma učebňa';
$string['nopapersfound'] = 'Neboli nájdené žiadne dokumenty!';
$string['nopapersfoundmsg'] = 'K tejto učebni nie je možné, v súčasnosti, nájsť žiadne dokumenty.';
$string['emergencynumbers'] = 'Čísla na pohotovosť';
$string['title'] = 'Nadpis';
$string['surname'] = 'Priezvisko';
$string['forenames'] = 'Krstné meno';
$string['extension_mins'] = 'Predĺženie (minúty)';
$string['endtime'] = 'Čas ukončenia';
$string['extendtime'] = 'Predĺženie';
$string['extendtimeby'] = 'Predĺženie času o';
$string['addnote']        = 'Pridať poznámku';
$string['toiletbreak'] = 'Prestávka';
$string['currenttime']    = 'Aktuálny čas:';
$string['start'] = 'Štart:';
$string['end']       = 'Koniec';
$string['start_but'] = 'Štart'; 
$string['endat_but']  = 'Koniec o';
$string['session_end'] = 'Koniec relácie';
$string['duration'] = 'Trvanie:';
$string['mins'] = 'minút';
$string['hour']      = 'hodina';
$string['hours']     = 'hodiny';
$string['papernote'] = 'Poznámky k testu';
$string['extratime'] = 'Predĺženie';
$string['preexam'] = 'Pred skúškou';
$string['preexamlist'] = '<ol>
    <li>Umiestnite k počítačom prihlasovacie údaje</li>
    <li>Ku každému počítaču položte prázdny papier</li>
    <li>Skontrolujte, že sa všetci adepti správne prihlásili</li>
    <li>Používateľom, ktorým sa nepodarilo prihlásiť prideľte \'Dočasný účet\'</li>
    <li><strong>Poznámka:</strong> Nezačínajte pred stanoveným časom</li>
    </ol>';
$string['midexam'] = 'V priebehu skúšky';
$string['midexamlist'] = '<ol>
    <li>V prípade akéhokoľvek nedostatku či problémov, kontaktujte, prosím, správcu</li>
    <li>Zaznamenajte IT/ľudské problémy jednotlivých adeptov (kliknite na meno adepta)</li>
    <li>Zaznamenajte prestávky (kliknite na meno adepta)</li>
    <li>Zaznamenajte problémy s testom/úlohami pomocou možnosti \'Pridať poznámku...\'.</li>
    </ol>';
$string['postexam'] = 'Po skúške';
$string['postexamlist'] = '<ol>
    <li><img src="../artwork/green_speech.gif" width="15" height="14" /> "Týmto skúška končí. Prejdite, prosím, na poslednú stránku a kliknite na \'Dokončiť\'."</li>
    <li><img src="../artwork/green_speech.gif" width="15" height="14" /> "Kliknite na \'Zavrieť okno\' a následne sa z  Rog&#333 odhláste."</li>
    <li>Pozbierajte prihlasovacie údaje</li>
    <li>Pozbierajte prázdne papiere</li>
    <li>Skontrolujte, či sú odhlásené <strong>všetky</strong> počítače</li>
    </ol>';
$string['time'] = 'Čas';
$string['timedexam'] = 'Skúška s časovým limitom:';
$string['timeerror'] = 'Chyba v čase';
$string['timeerrormsg'] = 'Skúška musí trvať minimálne %d minút.';
$string['examquestionclarifications'] = 'Vysvetlenie testovej úlohy';
$string['midexamclarifications'] = 'Vysvetlenie v priebehu testovania';
$string['examchecklist'] = 'Kontrolný zoznam skúšky';
$string['viewrubric'] = 'Prehľad rubriky';
$string['examrubric'] = 'Rubrika skúšky';
$string['unknowncomputer'] = 'Neznámi počítač';
$string['unknowncomputermsg'] = 'Počítač, ktorý používate je pre Rog&#333; nerozpoznateľný.<br />Kontaktujte, prosím, správcu.';
?>