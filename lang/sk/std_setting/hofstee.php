<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['hofstee'] = 'Hofstedeova metóda';
$string['integeronly'] = 'celé čísla';
$string['cohort'] = 'Súhrnné % študentov';
$string['correct'] = "Študenti\' známky (%)";
$string['passmark'] = 'Potrebná známka';
$string['distinction'] = 'S vyznamenaním';
$string['minpass'] = 'Min prejde';
$string['maxpass'] = 'Max prejde';
$string['minfail'] = 'Min neprejde';
$string['maxfail'] = 'Max neprejde';
$string['cutscore'] = 'Znížiť skóre ';
$string['cutpercent'] = 'Znížiť %';
$string['standardssetting'] = 'Nastavenie štandardov';
$string['cohortsize'] = 'Veľkosť skupiny';
$string['maximumscore'] = 'Max skóre';
$string['topquartile'] = 'Q3'; 
$string['median'] = 'Q2 (Medián)';
$string['lowerquartile'] = 'Q1'; 
$string['minimumscore'] = 'Min skóre';
$string['save'] = 'Uložiť';
$string['marking0'] = 'Dokument bez zmien';
$string['marking1'] = 'Určite potrebnú známku/S vyznamenaním v štandardnom nastavení.';
$string['marking2'] = 'Prispôsobiť dokument štandardnému nastaveniu';
$string['examnotfinished'] = 'Skúška nebola ukončená';
$string['notfinishedmsg'] = 'Nastaviť štandardy Hofstedeovej metódy nie je možné pred plánovaným dátumom ukončenia skúšky.';
?>