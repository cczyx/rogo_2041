<?php            //cz
require '../lang/' . $language . '/std_setting/std_set_shared.php';

$string['angoffmethod'] = 'Angoffova metóda';
$string['groupreview'] = 'Skupinové hodnotenie';
$string['percentagemsg'] = 'Pomocou svetlo modrého rozbaľovacieho zoznamu, vedľa každej úlohy, označte <strong>minimum</strong> študentov, ktoré očakávate, že odpovie správne.';
$string['warningmsg'] = ' = Recenzia sa líši o viac ako 10%';
$string['changedmsg'] = '<strong>Varovanie</strong>: Jedno, alebo viac individuálnych hodnotení, z ktorých je tvorené hodnotenie skupiny, sa zmenilo od doby, kedy sa hodnotenie tvorilo. Toto/tieto hodnotenie/a nie sú zahrnuté v priemerných hodnotách vyobrazených nižšie.';
$string['note'] = 'Poznámka';
$string['updatepassmark'] = 'Aktualizovať potrebnú známku';
$string['saveratings'] = 'Uložiť hodnotenie';
$string['screen'] = 'Obrazovka';

?>