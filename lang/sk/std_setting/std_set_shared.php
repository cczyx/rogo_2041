<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['standardssetting'] = 'Nastavenie štandardov';
$string['mark'] = 'Hodnotenie';
$string['marks'] = 'Hodnotenie';
$string['true'] = 'Pravda';
$string['false'] = 'Nepravda';
$string['yes'] = 'Áno';
$string['no'] = 'Nie';
$string['negmarking'] = 'Záporné známkovanie';
$string['bonusmark'] = 'Ku správnym odpovediam pridať %d bonus, %s pri celkom správnej odpovedi';
$string['mousereveal'] = '(Pre zobrazenie správnej odpovede prejdite myšou nad popis chyby)';
$string['ee_full'] = 'Ľahké - zásadné';
$string['ei_full'] = 'Ľahké - dôležité';
$string['en_full'] = 'Ľahké - doplnkové';
$string['me_full'] = 'Stredné - zásadné';
$string['mi_full'] = 'Stredné - dôležité';
$string['mn_full'] = 'Stredné - doplnkové';
$string['he_full'] = 'Ťažké - zásadné';
$string['hi_full'] = 'Ťažké - dôležité';
$string['hn_full'] = 'Ťažké - doplnkové';
$string['other'] = 'Ostatné';
$string['allitemscorrect'] = 'Všetky položky správne';
?>