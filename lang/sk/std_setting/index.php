<?php         //cz
$string['listsettings'] = 'Zoznam nastavení';
$string['standardssetting'] = 'Nastavenie štandardov';
$string['standardsetter'] = 'Nastavenie štandardu';
$string['date'] = 'Dátum';
$string['passscore'] = 'Potrebné skóre';
$string['distinction'] = 'S vyznamenaním';
$string['reviewmarks'] = 'Prehľad známok';
$string['papertotal'] = 'Prehľad dokumentu';
$string['method'] = 'Metóda';
$string['aboutstandardssetting'] = 'O nastavení štandardov';
$string['createmodifiedangoff'] = 'Nová modifikovaná Angoffova metóda';
$string['createebelmethod'] = 'Nová Ebelova metóda';
$string['editrating'] = 'Upraviť hodnotenie';
$string['delete'] = 'Odstrániť';
$string['modifiedangoff'] = 'Modifikovaná Angoffova metóda';
$string['individualrating'] = 'Individuálne hodnotenie';
$string['currentrating'] = 'Aktuálne hodnotenie';
$string['grouprating'] = 'Hodnotenie skupiny';
$string['createhofsteemethod'] = 'Nová Hofstedeova metóda';

?>