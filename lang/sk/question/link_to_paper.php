<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['linktopaper'] = 'Odkaz do dokumentu';
$string['addtopaper'] = 'Pridať do dokumentu';
$string['gotopaper'] = 'Prejsť na dokument ';
$string['cancel'] = 'Zrušiť';
$string['close'] = 'Zavrieť';
$string['back'] = 'Späť';
$string['warning'] = 'Varovanie';
$string['msg1'] = "Dokument je v súčasnej dobe \"aktívny\". Aktuálny dátum je uvedený medzi dátumom jeho začiatku a ukončenia. Tento bezpečnostný prvok teda zabraňuje úprave aktívneho dokumentu";
$string['msg2'] = 'Sumatívny dokument je v súčasnosti uzamknutý a nemôžu v ňom byť vykonávané žiadne zmeny.';
$string['newassessmentpaper'] = 'Nový dokument';
$string['success'] = 'Úloha pridaná';
$string['duplicatename'] = "Prepáčte <strong>'%s'</strong> názov je už použitý.";
$string['papernotonmodule'] = 'Mapovanie nie je možné skopírovať - cieľový test nie je v module mapovaných úloh';
?>