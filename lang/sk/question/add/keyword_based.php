<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../../lang/' . $language . '/include/status.inc';

$string['editor'] = 'Editor'; 
$string['changes'] = 'Zmeny';
$string['comments'] = 'Poznámky';
$string['mapping'] = 'Mapovanie';
$string['created'] = 'Vytvorené:';
$string['modified'] = 'Upravené:';
$string['metadata'] = 'Metadata'; 
$string['keywords'] = 'Kľúčové slová';
$string['teams'] = 'Tímy';
$string['mappingdisabled'] = 'Mapovanie vypnuté';
$string['questionnotonpaper'] = 'Táto úloha nie je v dokumente obsiahnutá, preto nemôže byť namapovaná';
?>