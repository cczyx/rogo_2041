<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['questioninformation'] = 'Informácie o úlohe';
$string['author'] = 'Autor';
$string['status'] = 'Stav'; 
$string['created'] = 'Vytvorená';
$string['modified'] = 'Upravená';
$string['locked'] = 'Uzamknutá';
$string['teams'] = 'Tímy';
$string['followingpapers'] = 'Použitá v následujúcich dokumentoch:';
$string['Longitudinal'] = 'Dlhodobá';
$string['normal'] = 'Normálna';
$string['retired'] = 'Neplatná';
$string['incomplete'] = 'Nedokončená';
$string['experimental'] = 'Pokusná';
$string['beta'] = 'Beta'; 
$string['close'] = 'Zavrieť';
$string['notused'] = '(Táto úloha nie je aktuálne použitá v žiadnom dokumente)';
$string['papername'] = 'Názov dokumentu';
$string['screenno'] = 'Obrazovka č.';
$string['examdate'] = 'Dátum skúšky';
$string['cohort'] = 'Skupina';
$string['p'] = 'P'; 
$string['d'] = 'D'; 
$string['copyof'] = 'Kópia z';
$string['sourcefor'] = 'Zdroj pre';
$string['na'] = 'N/A'; 
$string['type'] = 'Typ';
$string['questionno'] = 'Úloha č.';
$string['copies'] = 'Kópia:';
?>