<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['hypothesis'] = 'Hypotéza';
$string['investigation'] = 'Vyšetrenie';
$string['prescription'] = 'Preskripcia';
$string['intervention'] = 'Zákrok';
$string['treatment'] = 'Liečba';
$string['newinformation'] = 'Nová informácia';
$string['clinicalvignette'] = 'Klinický medailónik';
$string['thenthis'] = 'Potom';
$string['becomes'] = 'stane';
$string['is'] = 'je';
$string['of'] = 'z';
$string['experts'] = 'Experti';
?>