<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['copyontopaper'] = 'Kopírovať do dokumentu';
$string['cancel'] = 'Zrušiť';
$string['close'] = 'Zavrieť';
$string['gotopaper'] = 'Prejsť na dokument';
$string['back'] = 'Späť';
$string['warning'] = 'Varovanie';
$string['msg1'] = "Dokument je v současnej dobe \"aktívny\". Aktuálny dátum je uvedený medzi dátumom jeho začiatku a ukončenia. Tento bezpečnostný prvok teda zabraňuje úprave aktívneho dokumentu.";
$string['msg2'] = 'Sumatívny dokument je v současnosti uzamknutý a nemôžu v ňom býť vykonávané žiadne zmeny.';
$string['newassessmentpaper'] = 'Nový dokument hodnotenia';
$string['error1'] = "Súbor <strong>'%s'</strong> nemôže byť kopírovaný";
$string['error2'] = "Súbor <strong>'%s'</strong> nemôže byť kopírovaný.<br />Pôvodný názov bol: %s";
$string['error3'] = "Súbor <strong>'%s'</strong> nemôže byť kopírovaný.<br />Súbor nenájdený.";
$string['success'] = 'Úloha skopírovaná do <strong>%s</strong>.';
$string['duplicatename'] = "Prepáčte, <strong>'%s'</strong> názov je už použitý.";
$string['qcopyerrorno'] = 'Chyba pri kopírovaní úlohy';
$string['qcopyerror'] = 'Úloha %d nebola skopírovaná';
$string['papernotonmodule'] = 'Mapovanie nnie je možné skopírovať - cieľový test nie je v module mapovaných úloh ';
?>