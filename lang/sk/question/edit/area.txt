<?php        //cz
$string['But_Clear_All'] = 'Vymazať všetko';
$string['But_Delete_point'] = 'Odstrániť bod';
$string['But_your_answer'] = 'Vaša odpoveď';
$string['But_correct_answer'] = 'Správna odpoveď';
$string['But_show_error'] = 'Zobraziť chybu';
$string['Magnify'] = 'Zväčšiť';
$string['tt_Clear_All'] = 'Vymazať všetko';
$string['tt_Delete_point'] = 'Odstrániť bod';
$string['tt_your_answer'] = 'Vaša odpoveď';
$string['tt_correct_answer'] = 'Správna odpoveď';
$string['tt_show_error'] = 'Zobraziť chybu';
$string['tt_Magnify'] = 'Zväčšiť';
$string['Start_over'] = 'Začať znova';
$string['Eraser'] = 'Guma';
$string['popUp_msg'] = 'Chcete vymazať všetky riadky?';
$string['popUp_yes'] = 'Áno';
$string['popUp_no'] = 'Nie';
$string['errorcanvas'] = 'Canvas nie je podporovaný';
$string['errorimagesarea'] = 'Oblasť úlohy nie je možné zobraziť, keďže sa niektoré obrázky nenačítali.';

?>