<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

// Likert Scales data - this is how the settings are saved, separate columns with vertical bars (<br /> tages can be used for break lines)
$string['failpass3'] = 'Neuspel|Na hrane|Uspel';
$string['lowhigh3'] = 'Málo||Veľa';
$string['lowhigh4'] = 'Málo|||Veľa';
$string['lowhigh5'] = 'Málo||||Veľa';
$string['neveralways3'] = 'Nikdy||Zakaždým';
$string['neveralways4'] = 'Nikdy|||Zakaždým';
$string['neveralways5'] = 'Nikdy||||Zakaždým';
$string['disagre3'] = 'Nesúhlasím|Neutrálny|Súhlasím';
$string['disagre4'] = 'Úplne<br />nesúhlasím|Nesúhlasím|Súhlasím|Úplne<br />súhlasím';
$string['disagre5a'] = 'Úplne<br />nesúhlasím|Nesúhlasím|Ani nesúhlasím<br />ani súhlasím|Súhlasím|Úplne<br />súhlasím';
$string['disagre5b'] = 'Úplne<br />nesúhlasím|Nesúhlasím|Neviem|Súhlasím|Úplne<br />súhlasím';
$string['disagre5c'] = 'Úplne<br />nesúhlasím|Nesúhlasím|Neutrálny|Súhlasím|Úplne<br />súhlasím';
// Likert Scales
$string['failpass'] = 'Neuspel, Na hrane, Uspel';
$string['lowhigh'] = 'Málo až Veľa';
$string['neveralways'] = 'Nikdy až Zakaždým';
$string['disagre3point'] = 'Nesúhlasím, Neutrálny, Súhlasím';
$string['disagre4point'] = 'Úplne nesúhlasím, Nesúhlasím, Súhlasím, Úplne súhlasím';
$string['disagre5pointneither'] = 'Úplne nesúhlasím, Nesúhlasím, Ani nesúhlasím ani súhlasím, Súhlasím, Úplne súhlasím';
$string['disagre5pointuncertain'] = 'Úplne nesúhlasím, Nesúhlasím, Neviem, Súhlasím, Úplne súhlasím';
$string['disagre5pointneutral'] = 'Úplne nesúhlasím, Nesúhlasím, Neutrálny, Súhlasím, Úplne súhlasím';
$string['custom'] = 'Vlastné';
$string['na_abstain'] = 'N/A|Zdržal sa|Nie je možné aplikovať';
// QMP for QTI import
$string['qmpagree3'] = 'Súhlas|Ani súhlas ani nesúhlas|Nesúhlas';
$string['qmplike3'] = 'Páči sa mi|Neutrálny|Nepáči sa mi';
$string['qmplikeme3'] = 'Ako ja|Neutrálny|Nie ako ja';
$string['qmpsatisfied3'] = 'Spokojný|Neutrálny|Nespokojný';
$string['qmpagree4'] = 'Úplne súhlasím|Súhlasím|Nesúhlasím|Úplne nesúhlasím';
$string['qmplike4'] = 'Páči sa mi veľmi|Páči sa mi| Nepáči sa mi|Veľmi se mi nepáči';
$string['qmplikeme4'] = 'Veľmi ako ja|Ako ja|Nie ako ja|Vôbec nie ako ja';
$string['qmpsatisfied4'] = 'Veľmi spokojný|Spokojný|Nespokojný|Veľmi nespokojný';
$string['qmpagree5'] = 'Úplne súhlasím|Súhlasím|Ani súhlas ani nesúhlas|Nesúhlasím|Úplne nesúhlasím';
$string['qmplike5'] = 'Veľmi sa mi páči|Páči sa mi|Neutrálny|Nepáči sa mi|Veľmi sa mi nepáči';
$string['qmplikeme5'] = 'Veľmi ako ja|Aako ja|Neutrálny|Nie ako ja|Vôbec nie ako ja';
$string['qmpsatisfied5'] = 'Veľmi spokojný|Spokojný|Neutrálny|Nespokojný|Veľmi nespokojný';
// True/false (OK, not strictly likert scale)
$string['qmptf'] = 'Pravda|Nepravda';
$string['qmpyn'] = 'Áno|Nie';

?>