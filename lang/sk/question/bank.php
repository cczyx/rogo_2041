<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/question_types.inc';
require_once '../lang/' . $language . '/include/paper_types.inc';
require_once '../lang/' . $language . '/include/blooms.inc';

$string['questionbank'] = 'Banka úloh';
$string['bytype'] = 'podľa typu';
$string['byblooms'] = 'podľa Bloomovej taxonómie';
$string['bykeyword'] = 'podľa kľúčového slova';
$string['bystatus'] = 'podľa stavu';
$string['bydifficulty'] = 'podľa náročnosti';
$string['bydiscrimination'] = 'podľa diskriminácie';
$string['byperformance'] = 'podľa výkonu';
$string['byobjective'] = 'podľa výučbových cieľov';
$string['manageobjectives'] = 'Správa cieľov';
$string['managekeywords'] = 'Správa kľúčových slov';
$string['referencematerial'] = 'Referenčný materiál';
$string['nokeywords'] = 'Do tohto modulu neboli pridané žiadne kľúčové slová.';
$string['question'] = 'Úloha';
$string['questions'] = 'Úlohy';
$string['papers'] = 'Papers';
$string['people'] = 'People';
$string['noquestions'] = 'V banke neboli nájdené žiadne úlohy.';
$string['noquestionsbloom']  = 'Nebola nájdená žiadna úloha na základe vyhľadávania "podľa Bloomovej taxonómie".';
$string['noquestionsstatus'] = 'Nebola nájdená žiadna úloha na základe vyhľadávania "podľa stavu".';
$string['noquestionskeyword'] = 'No questions found in bank by keyword.';
$string['noquestionsperformance'] = 'Nebola nájdená žiadna úloha na základe vyhľadávania "podľa výkonu".';
$string['noquestionsobjective'] = 'Nebola nájdená žiadna úloha na základe vyhľadávania "podľa cieľov".';
$string['search'] = 'Hľadať';
?>