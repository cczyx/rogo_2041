<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/status.inc';
require_once '../lang/' . $language . '/include/blooms.inc';

$string['question'] = 'Úloha';
$string['questionbank'] = 'Banka úloh';
$string['notinteam'] = 'Varovanie: nie je v tíme';
$string['type'] = 'Typ';
$string['owner'] = 'Vlastník';
$string['modified'] = 'Upravené';
$string['myquestionsonly'] = 'len moje úlohy';
$string['search'] = 'Hľadať';
$string['clear'] = 'Vyčistiť';
$string['createnewquestion'] = 'Nová úloha';
$string['quickview'] = 'Náhľad';
$string['editquestion'] = 'Upraviť úlohu';
$string['information'] = 'Informácie';
$string['copyontopaperx'] = 'Kopírovať do dokumentu';
$string['linktopaperx'] = 'Odkaz do dokumentu';
$string['deletequestion'] = 'Odstrániť úlohu';
$string['noquestionleadin'] = 'Varovanie: v hlavičke nie je text úlohy!';
$string['modulenomappings'] = 'Modul mapovanie cieľov neumožňuje';
$string['questiontype'] = 'Typ úlohy';
$string['bloomstaxonomy'] = 'Bloomova taxonómia';
$string['keyword'] = 'Kľúčové slovo';
$string['performance'] = 'výkon';
$string['learningoutcome'] = 'Výsledok výučby';
$string['showlockedquestions'] = 'Zobraziť uzamknuté úlohy';
$string['noquestions'] = 'V banke úloh neboly nájdené žiadne úlohy.';
?>