<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../../lang/' . $language . '/help/staff/index.php';
require '../../lang/' . $language . '/include/months.inc';

$string['dates'] = 'Data';
$string['page'] = 'Stránka';
$string['hits'] = 'Zobrazené';
$string['tutorial'] = 'Tutoriál';
$string['searches'] = 'Vyhľadávanie';
$string['term'] = 'Výraz';
$string['results'] = 'Výsledky';
$string['pagehits'] = 'Stránka zobrazená';
$string['tutorialstats'] = 'Štatistiky tutoriálu';
$string['filter'] = 'Filter';
$string['nohits'] = 'Žiadne zobrazenia';
$string['nosearches'] = 'Žiadne vyhľadania';
$string['notutorials'] = 'Žiadne tutoriály';
?>