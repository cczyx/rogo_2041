<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['performsummary'] = 'Prehľad výsledkov';
$string['scale'] = 'škála';
$string['boxplot'] = 'boxplot';
$string['noresults'] = 'Neboli nájdené žiadne výsledky skúšky.';

$string['examscript'] = 'Skript skúšky';
$string['objectives'] = 'Vzdelávacie ciele';
$string['personalcohortperformance'] = 'Výsledky jednotlivca/skupiny';
$string['jumptopaper'] = 'Prejsť na dokument';

$string['maximumscore'] = 'Max skóre';
$string['studentsposition'] = 'Pozícia študenta/ov';
$string['topquartile'] = 'Q3'; 
$string['median'] = 'Q2 (Medián)';
$string['lowerquartile'] = 'Q1'; 
$string['passmark'] = 'Potrebná známka';
$string['minimumscore'] = 'Min skóre';
$string['examname'] = 'Názov skúšky';
$string['studentsmark'] = 'Percenta študenta/ov ';
?>