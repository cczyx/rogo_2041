<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/module/index.php';

$string['staffmsg'] = 'Toto je Rog&#333; úvodná stránka určená pre študentov. Hľadali ste <a href="../staff/">stránku určenú zamestnancom</a>?';
$string['nopapers'] = 'V súčasnosti nemáte k dispozícii žiadne dokumenty.';
$string['to'] = 'do';
$string['objectivesbased'] = 'Komentáre podľa cieľov<br />v hodnotení na';
$string['questionsbased'] = 'Komentáre podľa úloh<br />v hodnotení na';
$string['screen'] = 'Obrazovka';
$string['screens'] = 'Obrazovky';
$string['eassessmentmanagementsystem'] = 'Systém elektronického testovania';
$string['passwordRequired'] = 'Vyžadované heslo';
$string['performsummary'] = 'Prehľad výsledkov';
?>