<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/paper/type.php';
require '../lang/' . $language . '/paper/new_paper2.php';

$string['marksloaded'] = 'Známky sú nahrané.';
$string['msg1'] = 'CSV súbor by mal obsahovať stĺpce v následujúcom poradí: ID študenta, Otázka 1, Otázka 2, ...';
$string['msg2'] = 'Vyberte, prosím, CVS súbor, ktorý chcete nahrať:';
$string['headerrow'] = 'Súbor obsahuje riadok so záhlavím.';
$string['errorsaving'] = 'Chyba pri aktualizácii loginov';
$string['notfound'] = 'Používateľské meno nebolo nájdené!';
$string['uploadmarks'] = 'Nahrať známky';
?>