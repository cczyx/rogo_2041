<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/paper/start.php';

$string['modelanswer'] = 'Model odpovedí:';
$string['notvisible'] = '<strong>Informácie:</strong> (kandidáti neuvidia)';
$string['reviewermsg'] = 'Toto je výpočtová úloha. Premenné sú vypočítané on-line a budú sa pre jednotlivých kandidátov líšiť. Odpoveď je však založená na jednom vzťahu. Kandidáti nevidia <strong>$A</strong>, atď. Uvidia iba náhodne generované čísla.';
$string['variable'] = 'premenná';
$string['generated'] = 'Generované';
$string['max'] = 'Maximum';
$string['min'] = 'Minimum';
$string['qcomments'] = 'Úl%d komentáre';
$string['questionok'] = 'Úloha OK';
$string['minorproblems'] = 'Nevýznamné/niekoľko problémov';
$string['majorproblems'] = 'Významné/mnoho problémov';
$string['cannotcomment'] = 'Nie je možné komentovať';
$string['deadlineexpired'] = 'Termín vypršal';
$string['deadlinepassed'] = 'Tu si môžete pozrieť test, ale nemôžete meniť svoje komentáre, termín ukončenia ani čas sprístupnenia skúšky.';
$string['confirmsubmit'] = 'Ukončili ste všetky úlohy v tomto okne, nebudete sa môcť vrátiť späť.\nSte si naozaj istý/á, že chcete pokračovať?';
$string['formula'] = 'Vzorec';
$string['tolerancefull'] = 'Odchýlka pre celkové hodnotenie';
$string['tolerancepartial'] = 'Odchýlka pre čiastočné hodnotenie';
$string['javacheck2'] = "Ste si naozaj istý/á, že chcete skončiť? Po kliknutí na tlačidlo 'OK' sa už nebudete môcť vrátiť.";
$string['msgselectable1'] = 'Vybraných príliš veľa možností! Je možné vybrať iba ';
$string['msgselectable2'] = ' položku/y/iek.';
$string['msgselectable3'] = 'Už ste vybrali ';
$string['msgselectable4'] = 'Vyberte, prosím,  iný ranking.';
$string['AllItemsCorrect'] = "Všetky položky správne";
$string['other'] = 'Ostatní';
$string['togglevariables'] = 'Prepínacie premenné';
?>