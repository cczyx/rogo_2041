<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/common.inc';
require '../lang/' . $language . '/question/sct_shared.php';

$string['notallowed'] = 'Prístup k tomuto dokumentu nie je momentálne povolený.';
$string['authfailed'] = 'Overenie zlyhalo.';
$string['sctreview'] = 'Revízia ZTS';
$string['name'] = 'Meno';
$string['email'] = 'E-mail';
$string['clinicalvignette'] = 'Klinický medailónik';
$string['newinformation'] = 'Nová informácia';
$string['note'] = 'POZNÁMKA:';
$string['sct_msg1'] = 'Potom je táto hypotéza:';
$string['sct_msg2'] = 'Potom je toto vyšetrenie:';
$string['sct_msg3'] = 'Potom je táto preskripcia:';
$string['sct_msg4'] = 'Potom je tento zákrok:';
$string['sct_msg5'] = 'Potom je táto liečba:';
$string['saved_msg'] = 'Vaše odpovede a odôvodnenia boli uložené. Môžete vykonať ďalšie zmeny, alebo ukončiť prácu zatvorením prehliadača.';
$string['top_msg'] = 'Toto okno je vytvorené pre Vašu ZTS(zhoda textu so scanárom) odpoveď. Stručne odôvodnite prečo sú Vaše odpovede správne.  <br /><br />  Vaše odpovede uložíte na server kliknutím na tlačidlo <strong>\'Uložiť\'</strong> dole v okne.';
$string['briefreasonwhy'] = 'Krátke odôvodnenie prečo?';
$string['nosctquestions'] = 'Nebola nájdená žiadna ZTS úloha';
$string['fireexit'] = 'Núdzový východ';
?>