<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['externalexaminerarea'] =  'Oblasť externého recenzenta';
$string['expired'] = '&lt;ukončená platnosť&gt; - môžete si zobraziť dokument a prečítať %s akcií/odpovedí na Vaše komentáre.';
$string['externalexamineraccess'] = 'Prístup externého recenzenta';
$string['notreviewed'] = 'Nerecenzované';
$string['reviewed'] = 'Recenzované: %s';
$string['msg1'] = 'Nižšie je uvedený zoznam dokumentov vyžadujúcich recenziu. Kliknutím na názov cvičenia, ju spustíte v novom okne. Ku každej úlohe sú, pre Vaše hodnotenie, k výberu tri tlačidlá: 1)<span style="background-color:#C0FFC0; background-image:url(\'../artwork/ok_background.png\'); background-repeat:repeat-x; color:#004000">&nbsp;Úloha&nbsp;OK&nbsp;</span> (Pôvodná), 2)<span style="background-color:#FFFFC0; background-image:url(\'../artwork/minor_background.png\'); background-repeat:repeat-x; color:#404000">&nbsp;Malé/nejaké&nbsp;problémy&nbsp;</span>, alebo 3)<span style="background-color:#FFC0C0; background-image:url(\'../artwork/major_background.png\'); background-repeat:repeat-x; color:#800000">&nbsp;Veľké/niekoľko&nbsp;problémov&nbsp;</span>. Je tu tiež textové pole pre názor, ak navrhujete konkrétne zlepšenia.';
$string['msg2'] = 'Navigačné tlačidlá slúžiace na pohyb medzi oknami nájdete v dolnej časti každej obrazovky. Správne odpovede všetkých úloh sú zobrazené v tomto režime zobrazenia (študenti obdržia dokumenty bez odpovedí). Dokument môžete spustiť opakovane. Komentáre budú automaticky uložené pri prechode mezi oknami a kliknutím na tlačidlo \'Hotovo\'.';
$string['msg3'] = 'Nižšie je uvedený zoznam testov, ktoré sú k dispozícii pre prieskum výkonu skupiny študentov. Na prvej obrazovke sú zobrazené známky z testu. Kliknutím na ikonku u ktoréhokoľvek študenta otvoríte menu, v ktorom sa nachádza skript skúšky. Tento skript zachytáva študentove presné odpovede.';
$string['nopapersfound'] = 'Neboli nájdené žiadne dokumenty!';
$string['copyrightmsg'] = 'Úlohy vytvorené Rogō sú chránené autorským právom Spojeného kráľovstva a sú vo vlastníctve %s.';
$string['helpandsupport'] = 'Pomoc a podpora';
$string['helpandsupportext'] = 'Nápoveda a podpora dokumentov hodnotenia pre oponenta v Rogō.';
$string['rogodetails'] = 'Rogō %s je open source e-hodnotenie, pod vedením Information Services na University of Nottingham.<br />Ďalšie informácie o Rogō nájdete na stránkach projektu:';
$string['onlinesupportsystem'] = 'Systém on-line podpory pre študentov';
$string['email'] = 'E-mail';
$string['deadline'] = 'Termín';
$string['notset'] = '&lt;nie je nastavený&gt;';
$string['preexamreviewpapers'] = 'Kontrola testu pred skúškou';
$string['postexamreviews'] = 'Kontrola testu po skúške';$string['papersforreview'] = 'Papers for Review';
$string['msg4'] = 'Below is a list of exam papers requiring review.  In the paper, beneath each question are four buttons for you to rate the question: 1) <span style="background-color:#C5E0B3">&nbsp;Question&nbsp;OK&nbsp;</span>, 2) <span style="background-color:#FFE599">&nbsp;Minor/some&nbsp;problems&nbsp;</span>, 3) <span style="background-color:#FF9090">&nbsp;Major/several&nbsp;problems&nbsp;</span> or 4) <span style="background-color:#C0C0C0">&nbsp;Cannot Comment&nbsp;</span> for any questions outside of your field of expertise. There is also a textbox to directly record your feedback to us where you feel there are points to raise.';

?>