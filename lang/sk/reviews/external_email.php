<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['emailtemplate'] = 'Emailová šablóna';
$string['to'] = 'Komu';
$string['cc'] = 'Kópia';
$string['bcc'] = 'Skrytá kópia';
$string['subject'] = 'Predmet';
$string['subject_msg0'] = 'Komentár k e-testu %s ';
$string['subject_msg1'] = 'PRIPOMIENKA: %s e-assessment review';
$string['subject_msg2'] = '%s review comments';
$string['emailsent'] = 'Email bol odoslaný.';
$string['back'] = '&lt späť';
$string['email'] = 'E-mail';
$string['noexaminers'] = 'Nebol nastavený žiaden termín (deadline) pre externých skúšajúcich.';
?>