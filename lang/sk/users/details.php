<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';
require '../lang/' . $language . '/include/titles.inc';

$string['usermanagement'] = 'Nastavenie používateľov';
$string['usersearch'] = 'Hľadanie používateľa';
$string['user'] = 'Používateľ:';
$string['edit'] = 'Editovať';
$string['status'] = 'Stav'; 
$string['student'] = 'Študent';
$string['Students'] = 'Študenti';
$string['standardssetter'] = 'Standards Setter';
$string['externalexaminer'] = 'Oponent';
$string['internalreviewer'] = 'Internal Reviewer';
$string['graduate'] = 'Absolvent';
$string['left'] = 'Zanechal štúdium';
$string['suspended'] = 'Prerušené štúdium';
$string['locked'] = 'Locked';
$string['Staff'] = 'Zamestnanec';
$string['externalauth'] = '[Použiť externú autentizáciu]';
$string['gender'] = 'Pohlavie';
$string['male'] = 'Muž';
$string['female'] = 'Žena';
$string['other'] = 'Iné';
$string['year'] = 'Rok';
$string['yearofstudy'] = 'Rok štúdia';
$string['course'] = 'Kurz';
$string['userid'] = 'Rog&#333; ID používateľa';
$string['log'] = 'Log';
$string['teams'] = 'Tímy';
$string['team'] = 'Tím ';
$string['modules'] = 'Moduly';
$string['notes'] = 'Poznámky';
$string['admin'] = 'Správca';
$string['accessibility'] = 'Prístupnosť';
$string['metadata'] = 'Metadata';
$string['papername'] = 'Názov dokumentu';
$string['type'] = 'Typ';
$string['started'] = 'Zahájené';
$string['duration'] = 'Trvanie';
$string['ipaddress'] = 'IP Addresa';
$string['update'] = 'Aktualizácia';
$string['telephone'] = 'Telefón';
$string['ldapunavailable'] = 'nedostupný LDAP Server';
$string['unknown'] = '&lt;neznámi&gt;';
$string['password'] = 'Heslo';
$string['email'] = 'E-mail';
$string['formative'] = 'Cvičný test';
$string['progresstest'] = 'Priebežný test';
$string['summative'] = 'Sumatívna skúška';
$string['survey'] = 'Prieskum';
$string['oscestation'] = 'Stanica OSCE';
$string['offlinepaper'] = 'Off-line dokument';
$string['peerreview'] = 'Recenzia';
$string['noassessmentstaken'] = '&lt;žiadny test/prieskum nebol vyplnený&gt;';
$string['moduleid'] = 'ID modulu';
$string['name'] = 'Názov';
$string['academicyear'] = 'Akademický rok';
$string['date'] = 'Dátum';
$string['paper'] = 'Dokument';
$string['note'] = 'Poznámka';
$string['author'] = 'Autor';
$string['newnote'] = 'Nová poznámka';
$string['extratime'] = 'Predĺženie';
$string['fontsize'] = 'Veľkosť písma';
$string['typeface'] = 'Typ písma';
$string['background'] = 'Farba pozadia';
$string['foreground'] = 'Farba písma';
$string['markscolour'] = 'Farba známok';
$string['themecolour'] = 'Farba nadpisu/motívu';
$string['labelscolour'] = 'Farba popisku';
$string['unanswered'] = 'Nezodpovedané';
$string['dismisscolor'] = 'Odstrániť farbu';
$string['medical'] = 'Lekársky';
$string['breaks'] = 'Pauzy';
$string['default'] = 'Pôvodné';
$string['noextratime'] = 'bez predĺženia';
$string['angledefault'] = '&lt;pôvodný&gt;';
$string['value'] = 'Hodnota';
$string['save'] = 'Uložiť';
$string['dateadded'] = 'Dátum pridania';
$string['editteams'] = 'Upraviť tímy...';
$string['editmodules'] = 'Upraviť moduly...';
$string['system'] = 'Systém';
$string['na'] = 'N/A';
$string['universitylecturer'] = 'Univerzitný lektor';
$string['universitylibrarian'] = 'Univerzitný knihovník';
$string['universityadmin'] = 'Univerzitný správca';
$string['universitytechnical'] = 'Univerzitný IT/technik';
$string['nhslecturer'] = 'NHS lektor/konzultant';
$string['nhsadmin'] = 'NHS správca';
$string['invigilator'] = 'Dohľad';
$string['inactivestaff'] = 'Neaktívny zamestnanec' ;
$string['sysadmin'] = 'Systémový správca';
$string['reset'] = 'Obnoviť';
$string['forcereset'] = 'Vynútiť obnovenie';
$string['classifiedinfo'] = 'Utajované informácie';
$string['custom'] = 'Rutina';
$string['Access Denied'] = 'Prístup zamietnutý';
$string['resitcandidate'] = 'Preradiť študenta (reparát)';

//Colour picker
$string['colour'] = 'Farba';
$string['themecolours'] = 'Farebné motívy';
$string['standardcolours'] = 'Štandardné farby';
$string['more'] = 'Viac...';

//demo
$string['demo1'] = 'Mestá';
$string['demo2'] = 'Ktoré z následujúcich, je európskym mestom?';
$string['demo3'] = 'Áno';
$string['demo4'] = 'Nie';
$string['demo5'] = 'Londýn';
$string['demo6'] = 'New York';
$string['demo7'] = 'Paríž';
$string['demo8'] = '3 známky';
?>