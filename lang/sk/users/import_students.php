<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';

$string['sendwelcomeemail'] = 'Zaslať používateľovi uvítací email';
$string['importstudents'] = 'Nahrať študentov';
$string['csvfile'] = 'CSV súbor:';
$string['import'] = 'Importovať';
$string['msg1'] = 'Rogō dokáže nahrať podrobnosti študentov a vytvárať nové účty z CVS súboru. Prvý riadok by mal obsahovať záhlavie obsahujúce nasledujúce polia:';
$string['msg2'] = "Ďalšie (nepovinné) polia 'Moduly' a 'Relácie' môžu byť pridané a použité k hromadnému zápisu nových študentov do daných modulov.";
$string['loading'] = 'Načítavanie...';
$string['followingerrors'] = 'Neboli pridaní žiadni používateľia, z dôvodu nasledujúcich chýb:';
$string['usersadded'] = 'používatelia pridaní';
$string['usersupdated'] = 'existujúcí požívatelia aktualizovaní';
$string['missingcolumn'] = 'Chýba stĺpec \'%s\' z importu, prosím doplňte.';
$string['finished'] = 'Dokončené';
$string['loadstudents'] = 'Rogō: Načítať študentov';
$string['emailmsg1'] = 'Vytvoriť nový používateľský účet';
$string['emailmsg2'] = 'Milá/ý';
$string['emailmsg3'] = 'Bol vytvorený nový používateľský účet, pomocou ktorého sa môžete prihlásiť do systému on-line elektronického testovania Rogō. Vaše osobné overovacie údaje sú totožné s univerzitným prihlasovacím menom.';
$string['emailmsg4'] = 'Poznámka:';
$string['emailmsg5'] = 'Nikdy sa s nikým o Vaše univerzitné používateľské meno a heslo nedeľte.';
$string['emailmsg6'] = 'Opisovanie pri sumatívnych skúškach je akademickým priestupkom a nebude tolerované.';
$string['emailmsg7'] = 'Odoslanie e-mailu zlyhalo.';
?>