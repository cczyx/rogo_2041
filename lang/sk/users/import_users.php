<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';

$string['sendwelcomeemail'] = 'Poslať používateľovi uvítací e-mail';
$string['csvfile'] = 'CSV súbor:';
$string['import'] = 'Importovať';
$string['msg1'] = 'Nové účty používateľov (zamestnancov alebo študentov) môžu buť vytvorené z CSV súborov. Prvý riadok by mal obsahovať záhlavie, obsahujúce nasledujúce polia:';
$string['msg2'] = "Ďalšie (nepovinné) polia 'Moduly' a 'Relácie' možu byť pridané a použité k hromadnému zápisu nových študentov do daných modulov.";
$string['loading'] = 'Nahrávanie...';
$string['followingerrors'] = 'Neboli pridaní žiadni používatelia, z dôvodu nasledujúcich chýb:';
$string['usersadded'] = 'Používatelia pridaní';
$string['usersupdated'] = 'Existujúci používatelia aktualizovaní';
$string['missingcolumn'] = 'Chýbajúci \'%s\' stĺpec importného súboru, pridajte ho prosím.';
$string['finished'] = 'Dokončené';
$string['emailmsg1'] = 'Vytvorený nový používateľský účet';
$string['emailmsg2'] = 'Vážený/á';
$string['emailmsg3'] = 'Bol vytvorený nový účet pre prístup do systému on-line hodnotenia a prieskumov Rogō. Vaše prihlasovacie údaje sú rovnaké ako je Vaše univerzitné prihlásenie.';
$string['emailmsg4'] = 'Poznámka';
$string['emailmsg5'] = 'Nikdy sa s nikým o Vaše univerzitné používateľské meno a heslo nedeľte.';
$string['emailmsg6'] = 'Odpisovanie pri sumatívnych skúškach je akademickým priestupkom a nebude tolerované.';
$string['emailmsg7'] = 'Odoslanie e-mailu zlyhalo.';
$string['specifyfile'] = 'Určte, prosím, ktorý súbor nahrať.';
?>