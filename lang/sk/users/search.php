<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';
require '../lang/' . $language . '/include/titles.inc';

$string['usermanagement'] = 'Nastavenie používateľov';
$string['usersearch'] = 'Hľadanie používateľov';
$string['msg1'] = "<strong>Pozor</strong>&nbsp;&nbsp;&nbsp; nemáte žiadnu špecifikáciu kategórie hľadania v parametroch 'Rozšírené'.";
$string['msg2'] = 'V rámci zadananých kritérií nebol nájdený žiadny zodpovedajúci používateľ';
$string['performsummary'] = 'Prehľad výsledkov';
$string['largeresult'] = 'Nájdený veľký počet používateľov, zobrazených je len prvých 10 000.'
  . ' Prosím, pokúste sa spresniť Vaše kritériá hľadania.';
?>