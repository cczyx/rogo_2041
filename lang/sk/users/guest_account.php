<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require './lang/' . $language . '/include/common.inc';

$string['guestaccount'] = 'Dočasný účet';
$string['allocatedaccount'] = 'Pridelené účty';
$string['username'] = 'Používateľské meno';
$string['password'] = 'Heslo';
$string['login'] = 'Používateľské meno';
$string['guestaccountreg'] = 'Registrácia dočasného účtu';
$string['title'] = 'Titul';
$string['firstname'] = 'Meno';
$string['surname'] = 'Priezvisko';
$string['studentid'] = 'ID študenta';
$string['denied_msg'] = 'Táto stránka je dostupná len z univerzitného počítača v skúškovej miestnosti.';
$string['cannotfindexams'] = 'Rogō žiadnu skúšku nanašlo';
$string['msg'] = 'Zapíšte si prihlasovacia údaje uvedené nižšie, pre prípad, že budete nútený počas skúšky počítač reštartovať.';
$string['enterfirstname'] = "Zadajte, prosím, svoje meno.";
$string['entersurname'] = "Zadajte, prosím, svoje priezvisko.";
$string['error'] = 'Chyba';
$string['mandatory'] = 'Chýbajú povinné údaje.';
?>