<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';

$string['impmodtitle'] = 'Rogō: Nahrať moduly';
$string['csvfile'] = 'CSV súbor:';
$string['import'] = 'Importovať';
$string['msg1'] = "Rogō môže zápisy študentov imporotvať do modulu hromadne. Prvý riadok by mal obsahovať hlavičku obsahujúcu následujúce polia:";
$string['msg2'] = "Ďalšie (nepovinné) pole 'Pokus' môže byť použité k uloženiu čísla pokusu pre prípadné opakované vyplnenie. Ak pole nevyplníte, predpokladá sa, že sa jedná o prvý pokus.";
$string['addingmodules'] = 'Pridať moduly z';
$string['missingusers'] = 'Chýbajúci používateľia';
$string['missingmodules'] = 'Chýbajúce moduly';
$string['enrolementsperformed'] = 'Zápisy vykonané';
?>