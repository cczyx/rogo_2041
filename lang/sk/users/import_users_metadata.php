<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/module/index.php';

$string['importmetadata'] = 'Nahrať metadata študenta';
$string['msg'] = 'CSV súbor by mal obsahovať hlavičku obsahujúcu názvy polí. V prvom stĺpci by malo byť používateľské meno nasledované údajmi. Príklad nám ukazuje dve položky metadat (\'Site\' and \'Project Group\').';
$string['year'] = 'Rok';
$string['file'] = 'Súbor';
$string['import'] = 'Importovať';
$string['uploadedcorrectly'] = 'Nahraté správne.';
$string['notrecognised'] = 'Používateľské mená neboli rozpoznané:';
$string['loadingdata'] = 'Nahrávanie údajov...';
?>