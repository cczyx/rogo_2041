<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['resetpassword'] = 'Zmena hesla';
$string['notokensupplied'] = 'Token nie je podporovaný ';
$string['emailaddress'] = 'E-mailová adresa';
$string['password'] = 'Heslo';
$string['passwordconfirm'] = 'Potvrdenie hesla';
$string['emailaddressinvalid'] = 'Zadajte, prosím, platnú e-mailovú adresu';
$string['passwordsnotmatch'] = 'Heslo nie je zhodné';
$string['usernotfound'] = 'Používateľ nebol nájdený';
$string['incorrectemail'] = 'Bola zadaná chybná e-mailová adresa';
$string['databaseupdateerror'] = 'Databázová chyba pri aktualizácii hesla';
$string['login'] = 'Prihlásiť';
$string['pleaseenterpassword'] = 'Prosím, zadajte heslo';
$string['pleaseconfirmpassword'] = 'Prosím, potvrďte heslo';
$string['enternewpassword'] = 'Zadajte nové heslo.';
$string['confirmpassword'] = 'Potvrdenie nového hesla';
$string['reset'] = 'Obnoviť';
$string['passwordupdated'] = 'Heslo zmenené.';
?>