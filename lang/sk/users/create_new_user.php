<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require $cfg_web_root . '/lang/' . $language . '/include/user_search_options.inc';
require $cfg_web_root . '/lang/' . $language . '/include/titles.inc';

$string['createnewuser'] = 'Nový používateľ';
$string['getldapdetails'] = 'Získať detaily LDAP ';
$string['lastname'] = 'Priezvisko';
$string['firstnames'] = 'Meno';
$string['email'] = 'E-mail';
$string['password'] = 'Heslo';
$string['username'] = 'Používateľské meno';
$string['yearofstudy'] = 'Rok štúdia';
$string['typecourse'] = 'Rola/Kurz';
$string['gender'] = 'Pohlavie';
$string['studentid'] = 'ID študenta';
$string['onlyifstudent'] = '(len pre študentov)';
$string['sendwelcomeemail'] = 'Zaslať uvítací e-mail (vrátane hesla)';
$string['createaccount'] = 'Vytvoriť účet';
$string['male'] = 'Muž';
$string['female'] = 'Žena';
$string['other'] = 'Iné';
$string['universitystaff'] = 'Univerzitný zamestnanec';
$string['academiclecturer'] = 'Univerzitný lektor';
$string['administrator'] = 'Administrátor';
$string['ittechnical'] = 'IT/technik';
$string['externalstaff'] = 'Externý zamestnanec';
$string['nhslecturer'] = 'NHS lektor/konzultant';
$string['nhsadmin'] = 'NHS správca';
$string['externalexaminer'] = 'Oponent';
$string['internalreviewer'] = 'Internal Reviewer';
$string['standardssetter'] = 'Standards Setter';
$string['invigilator'] = 'Dohľad';
$string['students'] = 'Študent';
$string['newrogoaccount'] = 'Nový účet Rogō';
$string['rogoaccount'] = 'Rogō účet';
$string['newaccountcreated'] = 'Nový účet vytvorený pre';
$string['reqfirstname'] = "Zadajte, prosím, meno používateľa.";
$string['reqsurname'] = "Zadajte, prosím, priezvisko používateľa.";
$string['reqemail'] = "Zadajte, prosím, e-mailovú addresu používateľa.";
$string['reqcourse'] = "Zadajte, prosím, Role/Kurz používateľa.";
$string['requsername'] = "Zadajte, prosím, používateľské meno.";
$string['usernamechars'] = "Používateľské meno nemôže obsahovať podčiarknuté znaky.";
$string['reqpassword'] = "Zadajte, prosím, heslo používateľa.";
$string['usernameinuse'] = 'Používateľské meno \'%s\' je už používané. Zadajte, prosím, iné.';
$string['dear'] = 'Vážený/á';
$string['email1'] = 'Bol vytvorený nový prístupový účet do systému on-line hodnotenia a prieskumov Rogō. Detaily Vašej osobnej autentizácie sú:';
$string['email2'] = 'Pre prihlásenie do systému choďte na:';
$string['email3'] = 'Po prihlásení budete presmerovaný/á na osobnú obrazovku so zoznamom všetkých dokumentov, ktorým by ste mali venovať pozornosť.';
$string['casesensitive'] = '(rozlišuje "malé a VEĽKÉ)';
$string['couldnotsend'] = 'Odoslanie zlyhalo u týchto používateľov';
?>