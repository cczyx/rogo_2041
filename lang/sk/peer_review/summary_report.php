<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['reviewsummary'] = 'Prehľad recenzie'; 
$string['Review Form'] = 'Formulár recenzie';
$string['Student Profile'] = 'Profil študenta';
$string['title'] = 'Titul';
$string['surname'] = 'Priezvisko';
$string['firstnames'] = 'Krstné meno';
$string['studentid'] = 'ID študenta';
$string['reviewed'] = 'Hodnotené';
$string['reviews'] = 'Hodnotenia';
$string['overall'] = 'Celkovo';
$string['Complete'] = 'Hotovo';
$string['Missing'] = 'Chýba';
$string['q'] = 'Úl.';
?>