<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['norights'] = 'Na zobrazenie komentára nemáte oprávnenie.';
$string['examfeedback'] = 'Komentár ku skúške';
$string['feedback'] = 'Komentár';
$string['learningobjectives'] = 'Vzdelávacie ciele';
$string['explanation'] = 'Nižšie je uvedený zoznam konkrétnych vzdelávacích cieľov testovaných týmto dokumentom. Rovnaký cieľ môže byť testovaný vo viacerých úlohách, umožňuje čiastočné osvojenie si cieľa. Použite výsledky uvedené nižšie a venujte pozornosť červeným <img src="../artwork/major_comment.png" width="16" height="16" alt="Takmer nezískaný" /> a jantárovým <img src="../artwork/minor_comment.png" width="16" height="16" alt="Najčastejšie nenabudnutý" /> ciele, ktoré ste plne nezvládli.';
$string['notmapped'] = 'Tento dokument nebol namapovaný k žiadnemu vzdelávaciemu cieľu.';
$string['yourmark'] = 'Vaše hodnotenie';
$string['relative'] = 'Relatívne';
$string['qno'] = 'Počet úl.';
$string['objective'] = 'Cieľ';
$string['summaryinformation'] = 'Celkový prehľad';
$string['papertitle'] = 'Nadpis dokumentu';
$string['startedat'] = 'Začaté';
$string['examlength'] = 'Trvanie skúšky';
$string['timespent'] = 'Strávený čas';
$string['outof'] = 'mimo';
$string['adjusted'] = 'upravené';
$string['staffmsg'] = 'Ak chcete vidieť komentár ku konkrétnym študentom, použite, prosím, prístup cez celkový výkaz triedy.';
$string['greenicon'] = 'Nadobudnutie 80-100% z konkrétneho cieľa';
$string['ambericon'] = 'Nadobudnutie 50-79% z konkrétneho cieľa';
$string['redicon'] = 'Nadobudnutie 0-49% z konkrétneho cieľa';
$string['hyperlink'] = '<a href="" onclick="return false;">odkaz</a> - viac informácií získate v sekcii NLE';
$string['relativekey'] = "<strong>Relatívny</strong> - počet známok nad '+' alebo pod '-' vo vzťahu k priemeru skupiny";
$string['question'] = '<strong>Počet úl.</strong> - počet úloh namapovaných k cieľu';
$string['idmissing'] = 'ID študenta chýba';
$string['idmissing_msg'] = 'Nebolo predané žiadne študentské ID. Ak chcete vidieť komentár ku konkrétnym študentom, použite, prosím, prístup cez celkový výkaz triedy.';
?>