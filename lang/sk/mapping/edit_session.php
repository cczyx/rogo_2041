<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/mapping/sessions_list.php';
require '../lang/' . $language . '/include/months.inc';

$string['editsession'] = 'Úprava relácie';
$string['title'] = 'Nadpis';
$string['session'] = 'Relácia';
$string['date'] = 'Dátum';
$string['url'] = 'URL'; 
$string['new'] = 'Nová';
$string['save'] = 'Uložiť';
$string['manageobjectives'] = 'Správa cieľov';
$string['msg1'] = 'Zadajte nový cieľ';
$string['msg2'] = 'Zadajte, prosím, zmysluplný názov novej relácie.';
$string['entertitle'] = 'Zadajte, prosím, zmysluplný názov novej relácie.';
?>