<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/paper/details.php';

$string['start'] = 'Štart';
$string['owner'] = 'Vlastník';
$string['question'] = 'Úloha';
$string['type'] = 'Typ';
$string['marks'] = 'Známky';
$string['modified'] = 'Upravené';
$string['passmark'] = 'Potrebná známka';
$string['mappingbyquestion'] = 'Mapovanie podľa úloh';
$string['bysession'] = 'podľa relácie';
$string['byquestion'] = 'podľa úlohy';
$string['longitudinal'] = 'Dlhodobo';
$string['screen'] = 'Obrazovka';
$string['warning'] = 'Varovanie';
$string['nomatchsession'] = 'Relácia v názve dokumentu (%s) nezodpovedá dokumentu relácie (%s).';
$string['noquestiononscreen'] = '<strong>Varovanie:</strong> v okne nie sú žiadne úlohy.<br />Ak budete tento dokument využívať k testovaniu, objaví sa chyba!';
$string['questiononnotmap'] = '<strong>Varovanie:</strong> Táto úloha nie je v module namapovaná!';
?>