<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['sessions'] = 'Relácia';
$string['createsession'] = 'Nová relácia';
$string['importnle'] = 'Nahrať reláciu z %s';
$string['importfile'] = 'Nahrať reláciu zo súboru';
$string['editsession'] = 'Upraviť reláciu';
$string['deletesession'] = 'Odstrániť reláciu';
$string['copyyear'] = 'Kopírovať rok';
$string['date'] = 'Dátum';
$string['name'] = 'Meno';
$string['objectives'] = 'Vzdelávacie ciele';
$string['manageobjectives'] = 'Správa cieľov';
$string['msg1'] = 'Zdrojové a cieľové roky sa musia líšiť.';
$string['msg2'] = 'Vyberte, prosím, roky na kopírovanie z a do.';
$string['msg3'] = 'Neplatná hodnota v zozname rokov';
$string['msg4'] = 'Nie sú k dispozícii žiadne roky na kopírovanie';
$string['msg5'] = 'Kopírovanie cieľov z';
$string['msg6'] = 'Kopírovanie cieľov do';
$string['copy'] = 'Kopírovať';
?>