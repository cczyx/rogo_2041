<?php     //cz
require '../lang/' . $language . '/include/question_types.inc';
require '../lang/' . $language . '/include/paper_types.inc';

$string['papertasks'] = 'Možnosti dokumentu';
$string['testpreview'] = 'Náhľad testu';
$string['addquestionspaper'] = 'Pridať úlohy do dokumentu';
$string['editproperties'] = 'Upraviť vlastnosti';
$string['emailexternals'] = 'Zaslať email externistom';
$string['reports'] = 'Zostavy';
$string['mappedobjectives'] = 'Mapované ciele';
$string['importmarks'] = 'Nahrať známky';
$string['standardssetting'] = 'Nastavenie štandardov';
$string['importoscemarks'] = 'Import známok OSCE ';
$string['copypaper'] = 'Kopírovať dokument';
$string['deletepaper'] = 'Odstrániť dokument';
$string['retirepaper'] = 'Zneplatniť dokument';
$string['printhardcopy'] = 'Tlačená verzia';
$string['export12'] = 'Export 1.2';
$string['import'] = 'Importovať';
$string['Continuous'] = 'Úlohy plynule za sebou';
$string['Page-break per question'] = 'Jedna úloha na jednej strane';
$string['currentquestiontasks'] = 'Možnosti úlohy';
$string['editquestion'] = 'Upraviť úlohu';
$string['previewquestion'] = 'Náhľad úlohy';
$string['information'] = 'Informácie';
$string['copyontopaperx'] = 'Kopírovať do dokumentu';
$string['linktopaper'] = 'Odkaz do dokumentu';
$string['moveup'] = 'Presunúť vyššie';
$string['movedown'] = 'Presunúť nižšie';
$string['changescreenno'] = 'Zmeniť obrazovku č.';
$string['removefrompaper'] = 'Odstrániť z dokumentu';
$string['setkillerquestion'] = 'Nastaviť Killer Question';
$string['unsetkillerquestion'] = 'Zrušiť Killer Question';

$string['summativechecklist'] = 'Kontrolný zoznam';
$string['session'] = 'Relácia';
$string['mismatch'] = 'Nezhoda';
$string['examtime'] = 'Čas skúšky';
$string['incorrect'] = 'Chybne';
$string['incomplete'] = 'Nedokončená';
$string['duration'] = 'Dĺžka trvania';
$string['unset'] = 'Nenastavené';
$string['computerlabs'] = 'Počítačová učebňa';
$string['peerreviewed'] = 'Recenzované';
$string['peerreviewes'] = 'Interní recenzenti';
$string['externalreviews'] = 'Externí recenzenti';
$string['standardsset'] = 'Nastavenie Štandardov';
$string['mapping'] = 'Mapovanie';

$string['questionbanktasks'] = 'Správa banky úloh';
$string['questionsbytype'] = 'Úlohy podľa typu';
$string['questionsbyteam'] = 'Úlohy podľa tímov';
$string['questionsbykeyword'] = 'Úl. podľa kľúčových slov';
$string['search'] = 'Hľadať';
$string['createnewquestion'] = 'Nová úloha';

$string['dates'] = 'Údaje';
$string['to'] = 'k/do';
$string['course'] = 'Kurz';
$string['anycourse'] = '&lt;ľubovoľný kurz&gt;';
$string['module'] = 'Modul';
$string['anymodule'] = '&lt;ľubovoľný modul&gt;';
$string['cohort'] = 'Skupina';
$string['allcandidates'] = 'Všetci kandidáti';
$string['top'] = 'Najlepší';
$string['bottom'] = 'Najhorší';
$string['year'] = 'Rok';
$string['all'] = 'Všetko';
$string['reviews'] = 'Hodnotenia';
$string['internalpeerreview'] = 'Recenzia';
$string['externalexaminers'] = 'Externý recenzent';
$string['sctresponses'] = 'Zhoda so scénarom odpovede/dôvody';       
$string['classtotals'] = 'Celkové výkazy triedy';
$string['classtotalsexcel2003'] = 'Celkový výkaz triedy (Excel 2003)';
$string['classtotalscsv'] = 'Celkový výkaz triedy (CSV súbor)';
$string['textboxmarking'] = 'Hodnotenie textboxu';
$string['primarymarkbyquestion'] = 'Základné hodnotenie podľa úloh';
$string['selectpapersforremarking'] = 'Vybrať dokument k opätovnému známkovaniu';
$string['secondmarkbyquestion'] = 'Druhotné hodnotenie podľa úloh';
$string['finalisemarks'] = 'Dokončiť hodnotenie';
$string['frequencyanalysis'] = 'Frekvenčná &amp; diskriminačná (U-L) analýza';
$string['learningobjectiveanalysis'] = 'Analýza učebných cieľov';
$string['exportresponsescsvnum'] = 'Stiahnuť odpovede ako  CSV súbor (neupravené údaje)';
$string['exportresponsescsvtext'] = 'Stiahnuť odpovede ako CSV súbor (text)';
$string['exportbooleancsv'] = 'Stiahnuť odpovede v Boolovej algebre ako CSV';
$string['exportmarkscsv'] = 'Stiahnuť známky ako CSV súbor';
$string['individualportfoliosheets'] = 'Jednotlivé listy portfólia';
$string['exportratingscsv'] = 'Stiahnuť hodnotenie ako CSV súbor';
$string['ReviewSummary1'] = 'Revidovať prehľad - priemer (HTML)';
$string['ReviewSummary2'] = 'Revidovať prehľad - percenta (HTML)';
$string['ReviewSummary3'] = 'Revidovať prehľad';
$string['ReviewSummary4'] = 'Revidovať prehľad - priemer (CSV)';
$string['standardssettingcsv'] = "Standards Setting summary as CSV file";
$string['standardssettingfullcsv'] = "Standards Setting full responses as CSV file";
$string['xhtml'] = 'XHTML';
$string['word2003format'] = 'Formát Word 2003';
$string['rawdataxml'] = 'Neupravené údaje do XML súboru';
$string['rawdatacsv'] = 'Neupravené údaje do CSV súboru';
$string['itemanalysis'] = 'Položková analýza';
$string['cohortreports'] = 'Zostavy skupín';
$string['exports'] = 'Exporty';
$string['quantitativereports'] = 'Kvantitatívne zostavy';
$string['qualitativeanalysis'] = 'Kvalitatívna analýza';
$string['incabsentcandidates'] = 'vrátane chýbajúcich kandidátov';
$string['studentsonly'] = 'iba pokusy študentov';
$string['completedatasets'] = 'Zobraziť iba kompletné údajové sady';
$string['anyyear'] = 'Ľubovoľný rok';
$string['tooltip_studentattempts'] = 'Pre zobrazenie pokusov zamestnancov a študentov v súhrne triedy, zrušte zaškrtnutie políčka.';
$string['tooltip_daterange'] = 'Ku kontrole reportovacieho okna použite dátum.';

$string['copyname'] = 'Názov kópie:';
$string['paperonly'] = 'Iba dokument';
$string['paperandquestions'] = 'Dokument a úlohy';
$string['next'] = 'Ďalší &gt;&gt;';
$string['reorderinfo'] = 'Teraz môžete zmeniť poradie úloh a zalomenie obrazovky, ich pretiahnutím na novú pozíciu';
$string['insertscreenbreak'] = 'Vložiť zalomenie obrazovky';
$string['deletescreenbreak'] = 'Odstrániť zalomenie obrazovky';
$string['addscreenbreak'] = 'Pridať zalomenie obrazovky';
$string['midexamclarification'] = 'Objasnenie v priebehu skúšky';
$string['calculationquestions'] = 'Výpočtové úlohy';
$string['markallquestions'] = 'Označiť všetky úlohy';	
$string['exportraf'] = 'Exportovať ako Rog&#333; Assessment formát';
$string['importraf'] = 'Importovať ako Rog&#333; Assessment formát';
$string['importexport'] = 'Import/Export';
$string['papers'] = 'Dokumenty';
$string['namewarning'] = 'Tento názov je použitý v už existujúcom dokumente!';
$string['questions'] = 'Úlohy';
$string['people'] = 'Používatelia';
$string['type'] = 'Typ';
?>