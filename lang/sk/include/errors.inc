<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['error'] = 'Chyba';
$string['errormsg'] = 'Ak sa táto chyba opakuje, kontaktujte, prosím:';
$string['uploaderrormsg0'] = 'CHYBA - ';
$string['uploaderrormsg1'] = 'Bez problémov, súbor je nahraný.';
$string['uploaderrormsg2'] = 'Nahrávaný súbor bol väčší než upload_max_filesize v php.ini.';
$string['uploaderrormsg3'] = 'Nahrávaný súbor bol väčší než MAX_FILE_SIZE v html-form.';
$string['uploaderrormsg4'] = 'Súbor bol nahraný iba čiastočne.';
$string['uploaderrormsg5'] = 'Nebol nahraný žiadny súbor.';
$string['uploaderrormsg6'] = 'Žiadny dočasný adresár.';
$string['uploaderrormsg7'] = 'Neznámy problém.';
$string['fatalerrormsg0'] = 'Veľmi závažná chyba';
$string['fatalerrormsg1'] = 'Chýba povinná GET premenná.';
$string['fatalerrormsg2'] = 'Chýba povinná POST premenná.';
$string['fatalerrormsg3'] = 'Chýba povinná REQUEST premenná.';
$string['fatalerrorarray'] = 'Chýba premenná v povinnom poli.';
?>