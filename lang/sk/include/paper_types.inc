<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['formative self-assessment'] = 'Cvičné sebahodnotenie';
$string['formative quiz'] = 'Cvičný test';
$string['progress test'] = 'Priebežný test';
$string['summative exam'] = 'Sumatívna skúška';
$string['survey'] = 'Prieskum';
$string['osce station'] = 'Stanica OSCE';
$string['offline paper'] = 'Offline dokument';
$string['formative self-assessments'] = 'Cvičné sebahodnotenie';
$string['progress tests'] = 'Priebežné testy';
$string['summative exams'] = 'Sumatívne skúšky';
$string['surveys'] = 'Prieskumy';
$string['osce stations'] = 'Stanice OSCE';
$string['offline papers'] = 'Offline dokumenty';
$string['peer review'] = 'Recenzia';
$string['description0'] = 'Cvičné testy, ktoré by mali byť študentovi prístupné kedykoľvek.';
$string['description1'] = 'Bežne využívané na skúšanie, komentáre nie sú na konci testu pre študenta viditeľné.';
$string['description2'] = 'Rozhodné testy, ktorých hodnotenie sa podieľa na celkovej známke.';
$string['description3'] = 'Dotazník používaný k získaniu názorov a komentárov študentov.';
$string['description4'] = 'Predmetný OSCE (Objective Structured Clinical Examination) má využitie v medicínskych a zdravotníckych odboroch.';
$string['description5'] = 'Tento typ dokumentu umožňuje načítanie známok z offline testu do Rogo.';
$string['description6'] = 'Generuje študentom formulár pre ich vzájomné hodnotenie.';
?>