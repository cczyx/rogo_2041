<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['passwordrequired'] = 'Požadované heslo';
$string['specificpassword'] = 'K tomuto dokumentu existuje konkrétne heslo.';
$string['enterpw'] = 'Zadajte, prosím, heslo k tomuto dokumentu';
$string['pwcontinue'] = 'Pokračovať';
$string['denied_location'] = 'Prístup k tomuto dokumentu nie je, z Vášho aktuálneho pripojenia, povolený. ';
$string['error_time'] = 'Dokument, v ktorom sa pokúšate o prístup, je k dispozícii len medzi %s a %s';
$string['notregistered'] = '%s %s (%s) nie je registrovaný k <strong>%s</strong> v <strong>%s</strong>.';
$string['error_module'] = 'Tento dokument nie je priradený k žiadnemu modulu.';
$string['error_metadata'] = 'Metadáta používateľa nezodpovedajú <strong>%s: %s</strong>';
$string['alreadycompleted'] = 'Cvičenie už prebehlo <strong>%s</strong>.';
?>
