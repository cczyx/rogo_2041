<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['genmsg'] = 'Hľadanie podľa všetkých, alebo len vybraných kritérií.';
$string['name'] = 'Priezvisko';
$string['username'] = 'Používateľské ID';
$string['studentid'] = 'ID študenta';
$string['module'] = 'Modul';
$string['academicyear'] = 'Akademický rok';
$string['advanced'] = 'Rozšírené';
$string['students'] = 'Študent';
$string['graduates'] = 'Absolvent';
$string['leavers'] = 'Odstupujúci';
$string['leftuniversity'] = 'Opúšťa školu';
$string['suspended'] = 'Suspendovaný';
$string['locked'] = 'Locked';
$string['staff'] = 'Zamestnanec';
$string['staffadmin'] = 'Zamestnanec (Admin)';
$string['staffsysadmin'] = 'Zamestnanec (SysAdmin)';
$string['staffstudent'] = 'Zamestnanec a Študent'; 
$string['inactivestaff'] = 'Neaktívny zamestnanec' ;
$string['externalexaminers'] = 'Externí recenzenti';
$string['internalreviewers'] = 'Internal Reviewers';
$string['staffstandardssetter'] = 'Staff (Standards Setter)';
$string['invigilators'] = 'Dozorujúce osoby';
$string['back'] = 'Späť';
$string['search'] = 'Hľadať';
$string['usertasks'] = 'Možnosti používateľa';
$string['viewuserfile'] = 'Zobraziť používateľské súbory';
$string['createnewuser'] = 'Nový používateľ';
$string['emailuser'] = 'Zaslať používateľovi e-mail';
$string['importusers'] = 'Nahrať používateľa';
$string['importmodules'] = 'Nahrať moduly';
$string['anyyear'] = 'ľubovoľný rok';
$string['anymodule'] = '(ľubovoľný modul)';
$string['title'] = 'Titul';
$string['year'] = 'Rok';
$string['course'] = 'Kurz';
$string['na'] = 'N/A'; 
$string['unknown'] = '&lt;neznámi&gt;';
$string['unknowndegree'] = '&lt;neznámi titul&gt';
$string['mx'] = 'Mx';
$string['mr'] = 'Pán';
$string['dr'] = 'MUDr'; 
$string['miss'] = 'Slečna';
$string['mrs'] = 'Pani';
$string['ms'] = 'Pani';
$string['professor'] = 'Profesor';
$string['deleteuser'] = 'Odstrániť používateľa';
$string['performsummary'] = 'Prehľad výsledkov';
$string['clearltilinks'] = 'Vyčistiť LTI linky';
?>