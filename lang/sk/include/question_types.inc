<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['anytype'] = '(ľubovoľný typ)';
$string['alltypes'] = 'Všetky typy';
$string['branching'] = 'Vetvenie';
$string['info'] = 'Informačný blok';
$string['keyword_based'] = 'Úloha s kľúčovým slovom';
$string['keyword_based_short'] = 'S kľúčovým slovom';
$string['random'] = 'Blok náhodných úloh';
$string['random_short'] = 'Náhodný blok';
$string['calculation'] = 'Výpočtová úloha';
$string['dichotomous'] = 'Dichotomická úloha';
$string['extmatch'] = 'Rozšírená zhoda';
$string['blank'] = 'Doplňovacia úloha';
$string['flash'] = 'Úloha využívajúca Flash';
$string['hotspot'] = 'Úloha s aktívnym miestom v obrázku';
$string['labelling'] = 'Umiestnenie popisku';
$string['likert'] = 'Likertova stupnica';
$string['matrix'] = 'Matica';
$string['mcq'] = 'Výber z možností';
$string['mrq'] = 'Výber viac správnych odpovedí';
$string['rank'] = 'Poradie';
$string['sct'] = 'Test zhody so scenárom';
$string['sct_short'] = 'Zhoda so scenárom';
$string['textbox'] = 'Textové pole';
$string['true_false'] = 'Pravda/Nepravda';
$string['area'] = 'Plocha';
$string['enhancedcalc'] = 'Rozšírená výpočtová úloha';

$string['veryeasy'] = 'Veľmi jednoduchá';
$string['easy'] = 'Jednoduchá';
$string['moderate'] = 'Priemerná';
$string['hard'] = 'Ťažká';
$string['veryhard'] = 'Veľmi ťažká';
$string['mean'] = 'Priemer';
$string['highest'] = 'Najvyšší';
$string['high'] = 'Vysoké';
$string['intermediate'] = 'Stredné';
$string['low'] = 'Nízke';
?>