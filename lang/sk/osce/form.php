<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['osceform'] = 'OSCE formulár';
$string['overallclassification'] = 'Celková klasifikácia:';
$string['feedback'] = 'Komentár:';
$string['Access Denied'] = 'Prístup zamietnutý';
$string['save'] = 'Uložiť';
$string['paperavailable'] = 'Dokument, u ktorého sa pokúšate o prístup, je k dispozícii len v následujúcich časoch:';
$string['from'] = 'Z';
$string['to'] = 'Do';$string['marking3'] = array('Neuspel', 'Na hrane', 'Uspel');
$string['marking3'] = array('Neuspel', 'Na hrane', 'Uspel');
$string['marking4'] = array('Nedostatočne', 'Dostatočne', 'Dobre', 'Chválitebne', 'Výborne');
$string['marking5'] = array('Nepostačujúce', 'Postačujúce');
$string['marking6'] = array('Neuspel', 'Na hrane', 'Uspel', 'Uspel s vyznamenaním');
$string['marking7'] = array('Neuspel', 'Uspel');
$string['savefailed'] = 'Uloženie sa nepodarilo!';	
$string['tryagain'] = 'Prosím, skúste znova.';	
?>