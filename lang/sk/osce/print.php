<?php      //cz
$string['student'] = 'Študent:';
$string['examiner'] = 'Skúšajúci:';
$string['overallclassification'] = 'Celková klasifikácia';
$string['feedback'] = 'Komentár';
$string['msg'] = 'Označte, prosím, najvhodnejšie hodnotenie';
$string['clear fail'] = 'Neuspel';
$string['borderline'] = 'Na hrane';
$string['clear pass'] = 'Uspel';
$string['fail'] = 'Nedostatočne';
$string['borderline fail'] = 'Dostatočne';
$string['borderline pass'] = 'Dobre';
$string['pass'] = 'Chválitebne';
$string['good pass'] = 'Výborne';
$string['honours pass'] = 'Uspel s vyznamenaním';
?>