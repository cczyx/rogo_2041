<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['osceform'] = 'OSCE formulár';
$string['overallclassification'] = 'Celková klasifikácia';
$string['feedback'] = 'Komentár';
$string['clear fail'] = 'Neuspel';
$string['borderline'] = 'Na hrane';
$string['clear pass'] = 'Uspel';
$string['fail'] = 'Nedostatočne';
$string['borderline fail'] = 'Dostatočne';
$string['borderline pass'] = 'Dobre';
$string['pass'] = 'Chválitebne';
$string['good pass'] = 'Výborne';
$string['honours pass'] = 'Výborne s vyznamenaním';
$string['unsatisfactory'] = 'Nepostačujúce';
$string['competent'] = 'Postačujúce';
?>