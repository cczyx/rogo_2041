<?php           //cz
require '../lang/' . $language . '/include/question_types.inc';
require '../lang/' . $language . '/include/paper_types.inc';

$string['allmodules'] = 'Všetky moduly';
$string['bymodulecode'] = 'Moduly podľa kódu';
$string['admintools'] = 'Nastavenie';
$string['calendar'] = 'Kalendár';
$string['usermanagement'] = 'Nastavenie používateľov';
$string['makeafolder'] = 'Vytvoriť priečinok';
$string['mypersonalkeywords'] = 'Moje kľúčové slová';
$string['papertasks'] = 'Možnosti dokumentu';
$string['createnewpaper'] = 'Nový dokument';
$string['listpapers'] = 'Prehľad dokumentov';
$string['mymodules'] = 'Moje moduly';
$string['sysadminonly'] = 'Len systémový správca';
$string['adminonly'] = 'Len správca';
$string['create'] = 'Vytvoriť';
$string['newfolder'] = 'Nový priečinok';
$string['questionbanktasks'] = 'Správa banky úloh';
$string['questionsbytype'] = 'Úlohy podľa typu';
$string['questionsbyteam'] = 'Úlohy podľa tímov';
$string['questionsbykeyword'] = 'Úlohy podľa kľúčových slov';
$string['search'] = 'Hľadať';
$string['createnewquestion'] = 'Nová úloha';
$string['questions'] = 'Úlohy';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Používatelia';
?>