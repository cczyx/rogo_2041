<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['name'] = 'Popisný názov';
$string['oauth_consume_key']= 'OAuth spotrebiteľský kľúč';
$string['oauth_secret'] = 'OAuth bezpečnostný spotrebiteľský kľúč';
$string['oauth_context_id'] = 'Prepísať LTI kontextové ID';
$string['createLTIkeys'] = 'Nový LTI kľúč ';
$string['editLTIkeys'] = 'Upraviť LTI kľúč ';
$string['deleteLTIkeys'] = 'Odstrániť LTI kľúč ';
$string['ltikeys'] = 'LTI kľúče ';
$string['addltikeys'] = 'Pridať LTI kľúč';
$string['editltikeys'] = 'Upraviť LTI kľúč';
$string['mandatory'] = 'Označte <strong>povinné</strong> polia, ktoré musia byť vyplnené.';
$string['bymodulecode'] = 'Moduly podľa kódu';
$string['myfolders'] = 'Moje priečinky';
$string['papersoncurrentmodule'] = 'Dokumenty v tomto module';
$string['describemodulechoice'] = 'Vyberte, prosím, dokument k prelinkovaniu';
$string['missingfields'] ='Chýbajúce pole';
$string['module'] = 'Modul: %s';
?>