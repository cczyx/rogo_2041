<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require './lang/' . $language . '/include/question_types.inc';
require './lang/' . $language . '/include/paper_types.inc';

$string['admintools'] = 'Nastavenia';
$string['calendar'] = 'Kalendár';
$string['createfolder'] = 'Vytvoriť priečinok';
$string['mypersonalkeywords'] = 'Moje kľúčové slová';
$string['reviewed'] = 'Hodnotené';
$string['notreviewed'] = 'Nehodnotené';
$string['myfolders'] = 'Moje priečinky';
$string['mymodules'] = 'Moje moduly';
$string['unassignedpapers'] = 'Nepriradené dokumenty';
$string['recyclebin'] = 'Kôš';
$string['allmodules'] = 'Všetky moduly...';
$string['allmodulesinschool'] = 'Všetky moduly v škole...';
$string['myrecentpapers'] = 'Moje naposledy otvorené dokumenty';
$string['create'] = 'Vytvoriť';
$string['foldername'] = 'Názov priečinka';
$string['search'] = 'Hľadať';
$string['createnewquestion'] = 'Nová úloha';
$string['questions'] = 'Úlohy';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Ľudia';
$string['nomodulesset'] = 'Nie je nastavený žiadny modul';
$string['screen'] = 'Obrazovka';
$string['screens'] = 'Obrazovky';
$string['mins'] = 'minút';
$string['type'] = 'Typ';
$string['author'] = 'Autor';
$string['duplicatefoldername'] = 'Duplicitný názov priečinka, použite, prosím, iný.';
$string['nomodules'] = 'Nie ste členom žiadneho tímu. Pre získanie pomoci, prosím, kontaktujte:';
$string['papersforreview'] = 'Dokumenty na recenziu';
$string['deadline'] = 'Konečný termín:';
$string['recent'] = 'Naposledy otvorené';
$string['unassigned'] = 'Nepriradené';
$string['unassignedmsg'] = 'Úlohy/dokumenty nepriradené do modulu';
?>