<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['moduleselfenrolment'] = 'Modul Samoprihlasovania';
$string['moduleid'] = 'ID Modulu:';
$string['name'] = 'Meno:';
$string['school'] = 'Škola:';
$string['academicyear'] = 'Akademický rok:';
$string['enroll'] = 'Zápis';
$string['iwouldliketo'] = '%s %s %s (%s), chcete sa sám(a) zapísať do modulu.';
$string['notavailableselfenrollment'] = 'Tento modul nemá nastavené Samoprihlasovanie.';
$string['notactive'] = 'Tento modul nie je aktuálne aktívny.';
$string['icanaccess'] = 'Zobraziť dokumenty, ku ktorým mám prístup.';
$string['enrolmentcompleted'] = 'Zápis ukončený';
$string['nomodule'] = 'Modul s kódom <strong>%s</strong> nebol nájdený.';
?>