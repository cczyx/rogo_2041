<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/months.inc';
require_once '../lang/' . $language . '/paper/new_paper2.php';

$string['moreinformation'] = 'Podrobnejšie informácie';
$string['viewdetails'] = 'Zobraziť detaily';
$string['debuginformation'] = 'Debug informácií';
$string['loadingdebug'] = 'Načítať Debug';
$string['intermediateformatdebug'] = 'Stredný Debug formát';
$string['savingdebug'] = 'Uložiť Debug';
$string['generaldebuginfo'] = 'Všeobecné Debug Info';
$string['addingtopaper'] = 'Pridať k dokumentu';
$string['params'] = 'Parametre';
$string['othherdebug'] = 'Iný Debug';
$string['noquestions'] = 'Do Rogo neboli načítané žiadne úlohy';
$string['questiontables'] = 'Tabuľky úloh';
$string['questionsrow'] = 'riadok úlohy';
$string['optionsrows'] = 'riadky možností';
$string['newkeywords'] = 'nové kľúčové slová';
$string['addingquestiondetails'] = 'pridať úlohu %d ako display_pos = %d do okna %d';
?>