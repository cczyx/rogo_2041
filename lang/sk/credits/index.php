<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['credits'] = 'Kredity';
$string['msg'] = 'je vydané pod <a href="http://www.gnu.org/licenses/gpl.html" target="_blank">GPL v3.0</a> open source licenciou, pričom partneri môžu modifikovať, rozširovať a prispievať k projektu.';
$string['designprogramming'] = 'Dizajn &amp; Programovanie';
$string['languagepacks'] = 'Jazykové balíčky';
$string['editor'] = 'WYSIWYG Editor';
$string['javascriptlibrary'] = 'JavaScript knižnica';
$string['mp3player'] = 'MP3 Prehrávač';
$string['videoplayer'] = 'FLV Video Prehrávač';
$string['metadata'] = 'Metadata'; 
$string['graphics'] = 'Grafika';
$string['calculator'] = 'Kalkulačka';
$string['3rdparty'] = 'Subsystémy tretích strán';
$string['calcmsg'] = 'POZNÁMKA: Nie je open source, ale obsahuje oprávnenie';
?>