<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_options.inc';

$string['teammembers'] = 'Členovia tímu';
$string['edit'] = 'Upraviť';
$string['selfenrolmodule'] = 'Samoprihlasovací modul';
$string['studenturl'] = 'URL pre študentov';
$string['editpropertiessysadmin'] = 'Upraviť vlastnosti (SysAdmin)';
$string['manageobjectives'] = 'Správa Cieľov';
$string['managekeywords'] = 'Správa kľúčových slov';
$string['referencematerial'] = 'Referenčný materiál';
$string['listpapers'] = 'Zoznam testov';
$string['module'] = 'Modul';
$string['questionbank'] = 'Banka úloh';
$string['allquestions'] = 'Všetky úlohy';
$string['bykeyword'] = 'podľa kľúčového slova';
$string['byquestiontype'] = 'podľa typu úlohy';
$string['bystatus'] = 'podľa stavu';
$string['bybloom'] = 'podľa Bloomovej taxonómie';
$string['byperformance'] = 'podľa výkonu';
$string['byobjective'] = 'podľa cieľa';
$string['search'] = 'Vyhľadať';
$string['questions'] = 'Úloh';
$string['papers'] = 'Testy';
$string['type'] = 'Typ';
$string['users'] = 'Používatelia';
$string['newpaper'] = 'Nový dokument';
$string['newquestion'] = 'Nová úloha';
$string['forpapers'] = 'v testoch';
$string['forquestions'] = 'v úlohách';
$string['forusers'] = 'v používateľoch';
$string['studentlist'] = 'Zoznam študentov (%s)';
$string['students'] = 'študentov';
$string['addmetadata'] = 'Pridať metadata';
$string['extradataaboutstudents'] = 'Priradiť ďalšie údaje k študentom na %s';
$string['noteammembers'] = 'Nebol nájdený žiadny člen tímu.';
?>